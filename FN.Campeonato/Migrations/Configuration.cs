namespace FN.CampeonatoWeb.Migrations
{
    using FN.CampeonatoWeb.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<FN.CampeonatoWeb.Models.FNCampeonatoCtx>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(FN.CampeonatoWeb.Models.FNCampeonatoCtx context)
        {

       
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Estados.AddOrUpdate(
              p => p.Sigla,
              new Estado { Sigla = "SP" },
              new Estado { Sigla = "AC" },
              new Estado { Sigla = "AL" },
              new Estado { Sigla = "AP" },
              new Estado { Sigla = "AM" },
              new Estado { Sigla = "BA" },
              new Estado { Sigla = "CE" },
              new Estado { Sigla = "DF" },
              new Estado { Sigla = "ES" },
              new Estado { Sigla = "GO" },
              new Estado { Sigla = "MA" },
              new Estado { Sigla = "MT" },
              new Estado { Sigla = "MS" },
              new Estado { Sigla = "MG" },
              new Estado { Sigla = "PA" },
              new Estado { Sigla = "PB" },
              new Estado { Sigla = "PR" },
              new Estado { Sigla = "PE" },
              new Estado { Sigla = "PI" },
              new Estado { Sigla = "RJ" },
              new Estado { Sigla = "RN" },
              new Estado { Sigla = "RS" },
              new Estado { Sigla = "RO" },
              new Estado { Sigla = "RR" },
              new Estado { Sigla = "SC" },
              new Estado { Sigla = "SE" },
              new Estado { Sigla = "TO" }
            );
        }
    }
}
