namespace FN.CampeonatoWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inicio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Chave",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(),
                        Fase_Id = c.Int(nullable: false),
                        Fase_Id1 = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Fase", t => t.Fase_Id1)
                .Index(t => t.Fase_Id1);
            
            CreateTable(
                "dbo.Equipe",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Time_Id = c.Long(),
                        Chave_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Time", t => t.Time_Id)
                .ForeignKey("dbo.Chave", t => t.Chave_Id)
                .Index(t => t.Time_Id)
                .Index(t => t.Chave_Id);
            
            CreateTable(
                "dbo.Time",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Nome = c.String(),
                        Representante = c.String(),
                        Telefone1 = c.String(),
                        Telefone2 = c.String(),
                        Email = c.String(),
                        Endereco = c.String(),
                        Local_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Local", t => t.Local_Id)
                .Index(t => t.Local_Id);
            
            CreateTable(
                "dbo.Local",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Endereco = c.String(),
                        Telefone = c.String(),
                        Celular = c.String(),
                        Cidade_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cidade", t => t.Cidade_Id)
                .Index(t => t.Cidade_Id);
            
            CreateTable(
                "dbo.Cidade",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 255),
                        Estado_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Estado", t => t.Estado_Id, cascadeDelete: true)
                .Index(t => t.Estado_Id);
            
            CreateTable(
                "dbo.Estado",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Sigla = c.String(nullable: false, maxLength: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Fase",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DescricaoFase = c.Int(nullable: false),
                        Campeonato_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campeonato", t => t.Campeonato_Id, cascadeDelete: true)
                .Index(t => t.Campeonato_Id);
            
            CreateTable(
                "dbo.Campeonato",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(),
                        DataInicio = c.DateTime(nullable: false),
                        DataFim = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Integrante",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Nome = c.String(),
                        email = c.String(),
                        NumeroCamisa = c.Int(nullable: false),
                        DataNascimento = c.DateTime(nullable: false),
                        RG = c.String(),
                        CPF = c.String(),
                        Telefone = c.String(),
                        Celular1 = c.String(),
                        Celular2 = c.String(),
                        Endereco = c.String(),
                        Status = c.Int(nullable: false),
                        Cidade_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cidade", t => t.Cidade_Id)
                .Index(t => t.Cidade_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Integrante", "Cidade_Id", "dbo.Cidade");
            DropForeignKey("dbo.Chave", "Fase_Id1", "dbo.Fase");
            DropForeignKey("dbo.Fase", "Campeonato_Id", "dbo.Campeonato");
            DropForeignKey("dbo.Equipe", "Chave_Id", "dbo.Chave");
            DropForeignKey("dbo.Equipe", "Time_Id", "dbo.Time");
            DropForeignKey("dbo.Time", "Local_Id", "dbo.Local");
            DropForeignKey("dbo.Local", "Cidade_Id", "dbo.Cidade");
            DropForeignKey("dbo.Cidade", "Estado_Id", "dbo.Estado");
            DropIndex("dbo.Integrante", new[] { "Cidade_Id" });
            DropIndex("dbo.Chave", new[] { "Fase_Id1" });
            DropIndex("dbo.Fase", new[] { "Campeonato_Id" });
            DropIndex("dbo.Equipe", new[] { "Chave_Id" });
            DropIndex("dbo.Equipe", new[] { "Time_Id" });
            DropIndex("dbo.Time", new[] { "Local_Id" });
            DropIndex("dbo.Local", new[] { "Cidade_Id" });
            DropIndex("dbo.Cidade", new[] { "Estado_Id" });
            DropTable("dbo.Integrante");
            DropTable("dbo.Campeonato");
            DropTable("dbo.Fase");
            DropTable("dbo.Estado");
            DropTable("dbo.Cidade");
            DropTable("dbo.Local");
            DropTable("dbo.Time");
            DropTable("dbo.Equipe");
            DropTable("dbo.Chave");
        }
    }
}
