﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("Chave")]
    public class Chave
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        
        public int Fase_Id { get; set; }
        public virtual Fase Fase { get; set; }
        public virtual ICollection<Equipe> Equipes { get; set; }
    }
}