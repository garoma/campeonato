﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("Campeonato")]
    public class Campeonato
    {
        public int Id { get; set; }
        public string Descricao { get; set; }       
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public virtual ICollection<Fase> Fases { get; set; }
       
    }
}