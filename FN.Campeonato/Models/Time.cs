﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace FN.CampeonatoWeb.Models
{
    [Table("Time")]
    public class Time
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public Local Local { get; set; }
        public string Representante { get; set; }
        public string Telefone1 { get; set; }
        public string Telefone2 { get; set; }
        public string Email { get; set; }
        public string Endereco { get; set; }
    }
}