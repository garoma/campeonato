﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace FN.CampeonatoWeb.Models
{
    [Table("Fase")]
    public class Fase
    {
        public int Id { get; set; }
        public FaseEnum DescricaoFase { get; set; }        
        
        public int Campeonato_Id { get; set; }
        [ForeignKey("Campeonato_Id")]    
        public virtual Campeonato Campeonato { get; set; }
        public virtual ICollection<Chave> Chaves { get; set; }
    }
}