﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    public class FNCampeonatoCtx : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public FNCampeonatoCtx() : base("name=FNCampeonatoCtx")
        {
            
        }

        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Estado> Estados { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Cidade> Cidades { get; set; }

        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Chave> Chaves { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Equipe> Equipes { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Fase> Fases { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Integrante> Integrantes { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Local> Locais { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Time> Times { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Campeonato> Campeonatos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

      
    
    }
}
