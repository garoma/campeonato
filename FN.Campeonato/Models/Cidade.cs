﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FN.CampeonatoWeb.Models
{
    [Table("Cidade")]
    public class Cidade
    {
        public int Id { get; set; }
        [Required]
        [StringLength(255, ErrorMessage = "Nome deve conter no minímo 2 e no máximo 255 caracteres", MinimumLength = 2)]
        public string Nome { get; set; }
        
        public int Estado_Id { get; set; } 
        public virtual Estado Estado { get; set; } 

    }
}