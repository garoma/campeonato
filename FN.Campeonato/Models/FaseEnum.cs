﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FN.CampeonatoWeb.Models
{
    public enum FaseEnum
    {
    Classificação = 1,
    Oitavas = 2,
    Quartas = 3,
    Semi = 4,
    Final = 5
    }
}
