﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace FN.CampeonatoWeb.Models
{
    [Table("Local")]
    public class Local
    {
        public long Id { get; set; }
        public Cidade Cidade { get; set; }
        public string Endereco { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }

    }
}