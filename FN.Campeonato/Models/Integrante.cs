﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
namespace FN.CampeonatoWeb.Models
{
    [Table("Integrante")]
    public class Integrante
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public string email { get; set; }
        public int NumeroCamisa { get; set; }
        public DateTime DataNascimento { get; set; }
        public string RG { get; set; }
        public string CPF { get; set; }
        public string Telefone { get; set; }
        public string  Celular1 { get; set; }
        public string Celular2 { get; set; }
        public string Endereco { get; set; }
        public Cidade Cidade {get;set;}
        public Status Status { get; set; }

    }
}