﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FN.CampeonatoWeb.Models
{
    [Table("Estado")]
    public class Estado
    {
        public int Id { get; set; }
        [Required]
        [StringLength(2, ErrorMessage = "Sigla deve conter 2 caracteres", MinimumLength = 2)]
        public string Sigla { get; set; }
        [ForeignKey("Estado_Id")] 
        public virtual IList<Cidade> Cidades { get; set; } 
    }
}