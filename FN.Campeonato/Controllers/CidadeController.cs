﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;

namespace FN.CampeonatoWeb.Controllers
{
    public class CidadeController : Controller
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        // GET: /Cidade/
        public ActionResult Index(int? EstadoSelecionado)
        {
            
            var estados = db.Estados.OrderBy(q => q.Id).ToList();
            ViewBag.EstadoSelecionado = new SelectList(estados, "Id", "Sigla", EstadoSelecionado);
            int estadoId = EstadoSelecionado.GetValueOrDefault();
            var cidades = db.Cidades.Include(c => c.Estado)
            .Where(c => !EstadoSelecionado.HasValue || c.Estado_Id == estadoId)
            .OrderBy(d => d.Id);
            return View(cidades.ToList());
        }

        // GET: /Cidade/Details/5
        // 7 manilha
        //6 coração
        //8 manilha
        //7 de copas
        //6 de coração preto
        //8 copas

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cidade cidade = db.Cidades.Find(id);
            if (cidade == null)
            {
                return HttpNotFound();
            }
            return View(cidade);
        }

        // GET: /Cidade/Create
        public ActionResult Create()
        {
            ViewBag.Estado_Id = new SelectList(db.Estados, "Id", "Sigla");
            return View();
        }

        // POST: /Cidade/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Nome,Estado_Id")] Cidade cidade)
        {
            if (ModelState.IsValid)
            {
                db.Cidades.Add(cidade);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Estado_Id = new SelectList(db.Estados, "Id", "Sigla", cidade.Estado_Id);
            return View(cidade);
        }

        // GET: /Cidade/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cidade cidade = db.Cidades.Find(id);
            if (cidade == null)
            {
                return HttpNotFound();
            }
            ViewBag.Estado_Id = new SelectList(db.Estados, "Id", "Sigla", cidade.Estado_Id);
            return View(cidade);
        }

        // POST: /Cidade/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Nome,Estado_Id")] Cidade cidade)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cidade).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Estado_Id = new SelectList(db.Estados, "Id", "Sigla", cidade.Estado_Id);
            return View(cidade);
        }

        // GET: /Cidade/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cidade cidade = db.Cidades.Find(id);
            if (cidade == null)
            {
                return HttpNotFound();
            }
            return View(cidade);
        }

        // POST: /Cidade/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cidade cidade = db.Cidades.Find(id);
            db.Cidades.Remove(cidade);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
