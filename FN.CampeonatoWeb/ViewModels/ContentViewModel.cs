﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FN.CampeonatoWeb.ViewModel
{
    public class ContentViewModel
    {
        /// <summary>
        /// Busca ou seta um código identificador da entidade
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Busca ou seta o Título para o conteúdo 
        /// </summary>
        [Required]
        [DisplayName("Título")]
        public string Title { get; set; }
        /// <summary>
        /// Busca ou seta a descrição do conteúdo
        /// </summary>
        [Required]
        [DisplayName("Descrição")]
        public string Description { get; set; }
        /// <summary>
        /// Busca ou seta o Conteúdo
        /// </summary>
        [AllowHtml]
        [Required]
        [DisplayName("Conteúdo")]
        public string Contents { get; set; }
        /// <summary>
        /// Imagens
        /// </summary>
        [Required]
        [DisplayName("Imagem")]
        public byte[] Image { get; set; }
    }
}