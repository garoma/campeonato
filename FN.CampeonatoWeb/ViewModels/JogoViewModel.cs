﻿using FN.CampeonatoWeb.Models;
using System;
using System.Collections.Generic;

namespace FN.CampeonatoWeb.ViewModel
{
    public class JogoViewModel
    {
        // Dados Gerais
        public int JogoId { get; set; }
        public int EstadioId { get; set; }
        public List<int> ArbitrosSelecionados { get; set; }

        public string Data { get; set; }
        public string Hora { get; set; }
        public string Comentarios { get; set; }

        //Dados da Equipe da Casa
        public int EquipeCasaId { get; set; }
        public int ClubeCasaId { get; set; }
        public int GolsCasa { get; set; }
        public List<JogoEvento> EventosCasa { get; set; }

        //Dados da Equipe de Fora
        public int JogoEquipeForaId { get; set; }
        public int EquipeForaId { get; set; }
        public int ClubeForaId { get; set; }
        public int GolsFora { get; set; }
        public List<JogoEvento> EventosFora { get; set; }

        //Dados dos Eventos da Equipe da Casa
        public int AlteraEventoCasaId { get; set; }
        public int JogoCasaId { get; set; }
        public int JogoEquipeCasaId { get; set; }
        public int JogadorCasaId { get; set; }
        public int EventoCasaId { get; set; }
        public int MinutoCasa { get; set; }
        public int QuantidadeCasa { get; set; }
        public int TempoCasaId { get; set; }

        //Dados dos Eventos da Equipe de Fora
        public int AlteraEventoForaId { get; set; }
        public int JogoForaId { get; set; }
        public int JogadorForaId { get; set; }
        public int EventoForaId { get; set; }
        public int MinutoFora { get; set; }
        public int QuantidadeFora { get; set; }
        public int TempoForaId { get; set; }
    }
}