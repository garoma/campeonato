﻿using FN.CampeonatoWeb.Models;
using System;
using System.Collections.Generic;

namespace FN.CampeonatoWeb.ViewModel
{
    public class ClassificacaoViewModel
    {
        //Filtro
        public int TemporadaId { get; set; }
        public int FaseId { get; set; }
        public int GrupoId { get; set; }
    }
}