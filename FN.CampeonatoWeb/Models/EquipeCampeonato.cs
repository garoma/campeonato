﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("EquipeCampeonato")]
    public class EquipeCampeonato
    {
        [DisplayName("Código: ")]
        [Key]
        public int Id { get; set; }

        [DisplayName("Campeonato")]
        public int CampeonatoId { get; set; }
        [ForeignKey("CampeonatoId")]
        public virtual Campeonato Campeonato { get; set; }

        [DisplayName("Equipe")]
        public int EquipeId { get; set; }
        [ForeignKey("EquipeId")]
        public virtual Equipe Equipe { get; set; }

        public virtual List<Campeonato> Campeonatos { get; set; }
        public virtual List<Equipe> Equipes { get; set; }
        [NotMapped]
        public List<VisualizaLista> ListaDados { get; set; }
    }
}