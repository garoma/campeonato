﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace FN.CampeonatoWeb.Models
{
    public class DatabaseInitializer
    {
        public void CargaInicial()
        {
            using (FNCampeonatoCtx ctx = new FNCampeonatoCtx())
            {
                if (ctx.TiposCampeonatos.Count() <= 0)
                {
                    ctx.TiposCampeonatos.Add(new TipoCampeonato { Id = 0, Descricao = "Internacional" });
                    ctx.TiposCampeonatos.Add(new TipoCampeonato { Id = 0, Descricao = "Brasileiro" });
                    ctx.TiposCampeonatos.Add(new TipoCampeonato { Id = 0, Descricao = "Paulista" });
                    ctx.TiposCampeonatos.Add(new TipoCampeonato { Id = 0, Descricao = "Inter Copas" });
                    ctx.TiposCampeonatos.Add(new TipoCampeonato { Id = 0, Descricao = "Portugues" });
                    ctx.TiposCampeonatos.Add(new TipoCampeonato { Id = 0, Descricao = "Nacional" });
                    ctx.SaveChanges();
                }

                if (ctx.TiposEsportes.Count() <= 0)
                {
                    ctx.TiposEsportes.Add(new TipoEsporte { Id = 0, Descricao = "Fotebol" });
                    ctx.TiposEsportes.Add(new TipoEsporte { Id = 0, Descricao = "Volei" });
                    ctx.TiposEsportes.Add(new TipoEsporte { Id = 0, Descricao = "Rendibol" });
                    ctx.TiposEsportes.Add(new TipoEsporte { Id = 0, Descricao = "Basquete" });
                    ctx.TiposEsportes.Add(new TipoEsporte { Id = 0, Descricao = "Tenis" });
                    ctx.TiposEsportes.Add(new TipoEsporte { Id = 0, Descricao = "Natação" });
                    ctx.SaveChanges();
                }

                if (ctx.TiposEquipes.Count() <= 0)
                {
                    ctx.TiposEquipes.Add(new TipoEquipe { Id = 0, Descricao = "Profissional" });
                    ctx.TiposEquipes.Add(new TipoEquipe { Id = 0, Descricao = "Amador" });
                    ctx.TiposEquipes.Add(new TipoEquipe { Id = 0, Descricao = "Juvenil" });
                    ctx.TiposEquipes.Add(new TipoEquipe { Id = 0, Descricao = "Veterano" });
                    ctx.TiposEquipes.Add(new TipoEquipe { Id = 0, Descricao = "Mirim" });
                    ctx.TiposEquipes.Add(new TipoEquipe { Id = 0, Descricao = "Feminino" });
                    ctx.SaveChanges();
                }

                if (ctx.PosicoesJogadores.Count() <= 0)
                {
                    ctx.PosicoesJogadores.Add(new PosicaoJogador { Id = 0, Descricao = "Goleiro" });
                    ctx.PosicoesJogadores.Add(new PosicaoJogador { Id = 0, Descricao = "Lateral Esquerdo" });
                    ctx.PosicoesJogadores.Add(new PosicaoJogador { Id = 0, Descricao = "Lateral Direito" });
                    ctx.PosicoesJogadores.Add(new PosicaoJogador { Id = 0, Descricao = "Central" });
                    ctx.PosicoesJogadores.Add(new PosicaoJogador { Id = 0, Descricao = "Ponta Esquerda" });
                    ctx.PosicoesJogadores.Add(new PosicaoJogador { Id = 0, Descricao = "Ponta Direita" });
                    ctx.PosicoesJogadores.Add(new PosicaoJogador { Id = 0, Descricao = "Volante" });
                    ctx.PosicoesJogadores.Add(new PosicaoJogador { Id = 0, Descricao = "Centro Avante" });
                    ctx.SaveChanges();
                }

                if (ctx.Fases.Count() <= 0)
                {
                    ctx.Fases.Add(new Fase { Id = 0, Descricao = "Fase 01" });
                    ctx.Fases.Add(new Fase { Id = 0, Descricao = "Fase 02" });
                    ctx.Fases.Add(new Fase { Id = 0, Descricao = "Fase 03" });
                    ctx.Fases.Add(new Fase { Id = 0, Descricao = "Fase 04" });
                    ctx.Fases.Add(new Fase { Id = 0, Descricao = "Fase 05" });
                    ctx.SaveChanges();
                }

                if (ctx.Grupos.Count() <= 0)
                {
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  01" });
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  02" });
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  03" });
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  04" });
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  05" });
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  06" });
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  07" });
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  08" });
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  09" });
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  10" });
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  11" });
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  12" });
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  13" });
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  14" });
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  15" });
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  16" });
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  17" });
                    ctx.Grupos.Add(new Grupo { Id = 0, Descricao = "Grupo  18" });
                    ctx.SaveChanges();
                }

                if (ctx.Rodadas.Count() <= 0)
                {
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  01" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  02" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  03" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  04" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  05" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  06" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  07" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  08" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  09" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  10" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  11" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  12" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  13" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  14" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  15" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  16" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  17" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  18" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  19" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  20" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  21" });
                    ctx.Rodadas.Add(new Rodada { Id = 0, Descricao = "Rodada  22" });
                    ctx.SaveChanges();
                }

                if (ctx.Tempos.Count() <= 0)
                {
                    ctx.Tempos.Add(new Tempo { Id = 0, Descricao = "Primeiro" });
                    ctx.Tempos.Add(new Tempo { Id = 0, Descricao = "Segundo" });
                    ctx.Tempos.Add(new Tempo { Id = 0, Descricao = "Prorrogacao" });
                    ctx.Tempos.Add(new Tempo { Id = 0, Descricao = "Penaltis" });
                    ctx.SaveChanges();
                }
                if (ctx.Eventos.Count() <= 0)
                {
                    ctx.Eventos.Add(new Evento { Id = 0, Descricao = "Gol" });
                    ctx.Eventos.Add(new Evento { Id = 0, Descricao = "Cartao Vermelho" });
                    ctx.Eventos.Add(new Evento { Id = 0, Descricao = "Cartao Amarelo" });
                    ctx.Eventos.Add(new Evento { Id = 0, Descricao = "Falta" });
                    ctx.Eventos.Add(new Evento { Id = 0, Descricao = "Expulsao" });
                    ctx.Eventos.Add(new Evento { Id = 0, Descricao = "Suspensao" });
                    ctx.Eventos.Add(new Evento { Id = 0, Descricao = "Contusao" });
                    ctx.Eventos.Add(new Evento { Id = 0, Descricao = "chute a Gol" });
                    ctx.Eventos.Add(new Evento { Id = 0, Descricao = "Escanteio" });
                    ctx.Eventos.Add(new Evento { Id = 0, Descricao = "Impedimento" });
                    ctx.Eventos.Add(new Evento { Id = 0, Descricao = "Passe Errado" });
                    ctx.Eventos.Add(new Evento { Id = 0, Descricao = "Defesa" });
                    ctx.Eventos.Add(new Evento { Id = 0, Descricao = "Ataque" });
                    ctx.SaveChanges();
                }

                if (ctx.Campeonatos.Count() <= 0)
                {
                    ctx.Campeonatos.Add(new Campeonato { Id = 0, Descricao = "Campeonato Paulista", TipoCampeonatoId = 2, Pais = "Brasil" });
                    ctx.Campeonatos.Add(new Campeonato { Id = 0, Descricao = "Campeonato Brasileiro", TipoCampeonatoId = 2, Pais = "Brasil" });
                    ctx.Campeonatos.Add(new Campeonato { Id = 0, Descricao = "Campeonato Estadual", TipoCampeonatoId = 2, Pais = "Brasil" });
                    ctx.Campeonatos.Add(new Campeonato { Id = 0, Descricao = "Brasileirão", TipoCampeonatoId = 2, Pais = "Brasil" });
                    ctx.Campeonatos.Add(new Campeonato { Id = 0, Descricao = "Go FiFa Brasil", TipoCampeonatoId = 2, Pais = "Brasil" });
                    ctx.SaveChanges();
                }

                if (ctx.Temporadas.Count() <= 0)
                {
                    ctx.Temporadas.Add(new Temporada { Id = 0, Descricao = "2015", PontosVitoria = 2, PontosEmpate = 2, PontosVitoriaProrrogacao = 2, PontosEmpateProgrrogacao = 2, PontosDerrotaProrrogacao = 2, PontosVitoriaPenaltis = 2, PontosDerrotaPenaltis = 2, NumTimePromovido = 2, CriterioDesempate = "Saldo de Gols|Número de Vitórias|Gols Pró|Gols fora de Casa", NumTimeCompete1 = 2, NumTimeCompete2 = 2, NumTimeRebaixamento = 2, NumTimeRebaixado = 2 });
                    ctx.Temporadas.Add(new Temporada { Id = 0, Descricao = "2014", PontosVitoria = 2, PontosEmpate = 2, PontosVitoriaProrrogacao = 2, PontosEmpateProgrrogacao = 2, PontosDerrotaProrrogacao = 2, PontosVitoriaPenaltis = 2, PontosDerrotaPenaltis = 2, NumTimePromovido = 2, CriterioDesempate = "Saldo de Gols|Número de Vitórias|Gols Pró|Gols fora de Casa", NumTimeCompete1 = 2, NumTimeCompete2 = 2, NumTimeRebaixamento = 2, NumTimeRebaixado = 2 });
                    ctx.Temporadas.Add(new Temporada { Id = 0, Descricao = "2013", PontosVitoria = 2, PontosEmpate = 2, PontosVitoriaProrrogacao = 2, PontosEmpateProgrrogacao = 2, PontosDerrotaProrrogacao = 2, PontosVitoriaPenaltis = 2, PontosDerrotaPenaltis = 2, NumTimePromovido = 2, CriterioDesempate = "Saldo de Gols|Número de Vitórias|Gols Pró|Gols fora de Casa", NumTimeCompete1 = 2, NumTimeCompete2 = 2, NumTimeRebaixamento = 2, NumTimeRebaixado = 2 });
                    ctx.Temporadas.Add(new Temporada { Id = 0, Descricao = "2012", PontosVitoria = 2, PontosEmpate = 2, PontosVitoriaProrrogacao = 2, PontosEmpateProgrrogacao = 2, PontosDerrotaProrrogacao = 2, PontosVitoriaPenaltis = 2, PontosDerrotaPenaltis = 2, NumTimePromovido = 2, CriterioDesempate = "Saldo de Gols|Número de Vitórias|Gols Pró|Gols fora de Casa", NumTimeCompete1 = 2, NumTimeCompete2 = 2, NumTimeRebaixamento = 2, NumTimeRebaixado = 2 });
                    ctx.Temporadas.Add(new Temporada { Id = 0, Descricao = "2011", PontosVitoria = 2, PontosEmpate = 2, PontosVitoriaProrrogacao = 2, PontosEmpateProgrrogacao = 2, PontosDerrotaProrrogacao = 2, PontosVitoriaPenaltis = 2, PontosDerrotaPenaltis = 2, NumTimePromovido = 2, CriterioDesempate = "Saldo de Gols|Número de Vitórias|Gols Pró|Gols fora de Casa", NumTimeCompete1 = 2, NumTimeCompete2 = 2, NumTimeRebaixamento = 2, NumTimeRebaixado = 2 });
                    ctx.Temporadas.Add(new Temporada { Id = 0, Descricao = "2010", PontosVitoria = 2, PontosEmpate = 2, PontosVitoriaProrrogacao = 2, PontosEmpateProgrrogacao = 2, PontosDerrotaProrrogacao = 2, PontosVitoriaPenaltis = 2, PontosDerrotaPenaltis = 2, NumTimePromovido = 2, CriterioDesempate = "Saldo de Gols|Número de Vitórias|Gols Pró|Gols fora de Casa", NumTimeCompete1 = 2, NumTimeCompete2 = 2, NumTimeRebaixamento = 2, NumTimeRebaixado = 2 });
                    ctx.SaveChanges();
                }

                if (ctx.Arbitros.Count() <= 0)
                {
                    ctx.Arbitros.Add(new Arbitro { Id = 0, Nome = "Franklin", Nascimento = DateTime.Now, Tipo = 2, Foto = null });
                    ctx.Arbitros.Add(new Arbitro { Id = 0, Nome = "Feranndo", Nascimento = DateTime.Now, Tipo = 1, Foto = null });
                    ctx.Arbitros.Add(new Arbitro { Id = 0, Nome = "Fabiano", Nascimento = DateTime.Now, Tipo = 3, Foto = null });
                    ctx.Arbitros.Add(new Arbitro { Id = 0, Nome = "Flavio", Nascimento = DateTime.Now, Tipo = 2, Foto = null });
                    ctx.Arbitros.Add(new Arbitro { Id = 0, Nome = "Chiquinho", Nascimento = DateTime.Now, Tipo = 1, Foto = null });
                    ctx.Arbitros.Add(new Arbitro { Id = 0, Nome = "Sandro", Nascimento = DateTime.Now, Tipo = 4, Foto = null });
                    ctx.SaveChanges();
                }

                if (ctx.Jogadores.Count() <= 0)
                {
                    ctx.Jogadores.Add(new Jogador { Id = 0, PosicaoJogadorId = 1, EMail = "a@a.com", Nacionalidade = "brasileira", NumeroCamisa = 1, Apelido = "gordo", Nome = "Franklin", Nascimento = DateTime.Now, Foto = null });
                    ctx.Jogadores.Add(new Jogador { Id = 0, PosicaoJogadorId = 2, EMail = "a@a.com", Nacionalidade = "brasileira", NumeroCamisa = 10, Apelido = "tanga", Nome = "Feranndo", Nascimento = DateTime.Now, Foto = null });
                    ctx.Jogadores.Add(new Jogador { Id = 0, PosicaoJogadorId = 3, EMail = "a@a.com", Nacionalidade = "brasileira", NumeroCamisa = 5, Apelido = "biano", Nome = "Fabiano", Nascimento = DateTime.Now, Foto = null });
                    ctx.Jogadores.Add(new Jogador { Id = 0, PosicaoJogadorId = 4, EMail = "a@a.com", Nacionalidade = "brasileira", NumeroCamisa = 7, Apelido = "rodaque", Nome = "Flavio", Nascimento = DateTime.Now, Foto = null });
                    ctx.Jogadores.Add(new Jogador { Id = 0, PosicaoJogadorId = 5, EMail = "a@a.com", Nacionalidade = "brasileira", NumeroCamisa = 23, Apelido = "Chiquinho", Nome = "Weverly", Nascimento = DateTime.Now, Foto = null });
                    ctx.Jogadores.Add(new Jogador { Id = 0, PosicaoJogadorId = 6, EMail = "a@a.com", Nacionalidade = "brasileira", NumeroCamisa = 11, Apelido = "xamarin", Nome = "Sandro", Nascimento = DateTime.Now, Foto = null });
                    ctx.SaveChanges();
                }

                if (ctx.Estadios.Count() <= 0)
                {
                    ctx.Estadios.Add(new Estadio { Id = 0, Capacidade = 0, Foto = null, Pais = "Brasil", Nome = "Itaquerao", Descricao = "Itaquerao" });
                    ctx.Estadios.Add(new Estadio { Id = 0, Capacidade = 0, Foto = null, Pais = "Brasil", Nome = "Pacaembu", Descricao = "Pacaembu" });
                    ctx.Estadios.Add(new Estadio { Id = 0, Capacidade = 0, Foto = null, Pais = "Brasil", Nome = "Canecao", Descricao = "Canecao" });
                    ctx.SaveChanges();
                }

                if (ctx.Clubes.Count() <= 0)
                {
                    ctx.Clubes.Add(new Clube { Id = 0, EstadioId = 1, DataFundacao = DateTime.Now, Cidade = "Sao Paulo", Foto = null, Pais = "Brasil", Nome = "Corinthians", Descricao = "Corinthians" });
                    ctx.Clubes.Add(new Clube { Id = 0, EstadioId = 2, DataFundacao = DateTime.Now, Cidade = "Sao Paulo", Foto = null, Pais = "Brasil", Nome = "Palmeiras", Descricao = "Palmeiras" });
                    ctx.Clubes.Add(new Clube { Id = 0, EstadioId = 3, DataFundacao = DateTime.Now, Cidade = "Sao Paulo", Foto = null, Pais = "Brasil", Nome = "Santos", Descricao = "Santos" });
                    ctx.SaveChanges();
                }

                if (ctx.Equipes.Count() <= 0)
                {
                    ctx.Equipes.Add(new Equipe { Id = 0, Descricao = "Profissional Corinthians", ClubeId = 1, TipoEsporteId = 1, TipoEquipeId = 1 });
                    ctx.Equipes.Add(new Equipe { Id = 0, Descricao = "Profissional Palmeiras", ClubeId = 2, TipoEsporteId = 1, TipoEquipeId = 1 });
                    ctx.Equipes.Add(new Equipe { Id = 0, Descricao = "Profissional Santos", ClubeId = 3, TipoEsporteId = 1, TipoEquipeId = 1 });
                    ctx.Equipes.Add(new Equipe { Id = 0, Descricao = "Juvenil Palmeiras", ClubeId = 2, TipoEsporteId = 1, TipoEquipeId = 1 });
                    ctx.Equipes.Add(new Equipe { Id = 0, Descricao = "Veterano Santos", ClubeId = 3, TipoEsporteId = 1, TipoEquipeId = 1 });
                    ctx.Equipes.Add(new Equipe { Id = 0, Descricao = "Mirim Corinthians", ClubeId = 1, TipoEsporteId = 1, TipoEquipeId = 1 });
                    ctx.SaveChanges();
                }
            }
        }
    }
}