﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("Jogador")]
    public class Jogador
    {
        [DisplayName("Código")]
        [Key]        
        public int Id { get; set; }

        [DisplayName("Apelido")]
        [StringLength(30)]
        public string Apelido { get; set; }

        [DisplayName("Nome")]
        [StringLength(150)]
        public string Nome { get; set; }

        [DisplayName("Numero da Camisa")]
        public int NumeroCamisa { get; set; }

        [DisplayName("Posição do Jogador")]
        public int PosicaoJogadorId { get; set; }
        [ForeignKey("PosicaoJogadorId")]
        public virtual PosicaoJogador PosicaoJogador { get; set; }
        public virtual List<PosicaoJogador> PosicoesJogadores { get; set; }

        [DisplayName("Data Nascimento")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Nascimento { get; set; }

        [DisplayName("E-Mail")]
        [StringLength(150)]
        public string EMail { get; set; }

        [DisplayName("Nacionalidade")]
        [StringLength(50)]
        public string Nacionalidade { get; set; }

        [NotMapped]
        public string Idade { get { return string.Format("{0} Anos de Idade", (DateTime.Now.Year - Nascimento.Year)); } }

        [NotMapped]
        public string NascimentoF { get { return Nascimento.ToString("dd/MM/yyyy"); } }

        [DisplayName("Foto")]
        public byte[] Foto { get; set; }
    }
}