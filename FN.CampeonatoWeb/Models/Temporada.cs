﻿using FN.CampeonatoWeb.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("Temporada")]
    public class Temporada
    {
        [Key]
        [DisplayName("Código")]
        public int Id { get; set; }

        [StringLength(150)]
        [DisplayName("Temporada")]
        public string Descricao { get; set; }

        [DataType(DataType.MultilineText)]
        [DisplayName("Fórmula de Disputa")]
        public string FormulaDisputa { get; set; }

        [DefaultValue(3)]
        [DisplayName("Pontos por Vitória")]
        public int PontosVitoria { get; set; }

        [DefaultValue(1)]
        [DisplayName("Pontos por Empate")]
        public int PontosEmpate { get; set; }

        [DefaultValue(0)]
        [DisplayName("Pontos por Vitória")]
        public int PontosVitoriaProrrogacao { get; set; }

        [DisplayName("Pontos por Empate")]
        public int PontosEmpateProgrrogacao { get; set; }

        [DefaultValue(0)]
        [DisplayName("Pontos por Derrota")]
        public int PontosDerrotaProrrogacao { get; set; }

        [DefaultValue(0)]
        [DisplayName("Pontos por Vitória")]
        public int PontosVitoriaPenaltis { get; set; }

        [DefaultValue(0)]
        [DisplayName("Pontos por Derrota")]
        public int PontosDerrotaPenaltis { get; set; }

        [StringLength(100)]
        [DisplayName("Critério de Desempate")]
        public string CriterioDesempate { get; set; }

        [DefaultValue(0)]
        [DisplayName("Número de Equipes Promovidas")]
        public int NumTimePromovido { get; set; }

        [DisplayName("Número de Equipes Competição 1")]
        public int NumTimeCompete1 { get; set; }

        [DisplayName("Número de Equipes Competição 2")]
        public int NumTimeCompete2 { get; set; }

        [DefaultValue(0)]
        [DisplayName("Número Equipes Play-Off Rebaixamento")]
        public int NumTimeRebaixamento { get; set; }

        [DisplayName("Número de Equipes Rebaixadas")]
        public int NumTimeRebaixado { get; set; }

        [NotMapped]
        public List<System.Web.Mvc.SelectListItem> OpcoesCriterioDesempate
        { 
            get 
            {
                string[] criterios = new string[4];
                if (CriterioDesempate == null)
                    criterios = "Saldo de Gols|Número de Vitórias|Gols Pró|Gols fora de Casa".Split('|');
                else
                    criterios = CriterioDesempate.Split('|');

                List<System.Web.Mvc.SelectListItem> listitems = new List<System.Web.Mvc.SelectListItem>(criterios.Count());

                foreach (string s in criterios)
                {
                    listitems.Add(new System.Web.Mvc.SelectListItem()
                    {
                        Text = s,
                        Value = s
                    });
                }

                return listitems;
            }
        }
    }
}