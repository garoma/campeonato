﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("JogoArbitro")]
    public class JogoArbitro
    {
        [Key]
        [DisplayName("Código")]
        public int Id { get; set; }

        [DisplayName("Jogo")]
        public int JogoId { get; set; }
        [ForeignKey("JogoId")]
        
        public virtual JogoGeral JogoGeral { get; set; }

        [DisplayName("Arbitro")]
        public int ArbitroId { get; set; }
        [ForeignKey("ArbitroId")]
        public virtual Arbitro Arbitro { get; set; }
    }
}