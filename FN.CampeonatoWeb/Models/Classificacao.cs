﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("Classificacao")]
	public class Classificacao
	{
        [DisplayName("Código: ")]
        [Key]
        public int Id { get; set; }

        [DisplayName("Temporada")]
        public int TemporadaId { get; set; }
        [ForeignKey("TemporadaId")]
        public virtual Temporada Temporada { get; set; }

        [DisplayName("Data do Jogo")]
        public DateTime DataJogo { get; set; }

        public int EquipeId { get; set; }
        [ForeignKey("EquipeId")]
        public virtual Equipe Equipe { get; set; }

        [DefaultValue(0)]
        public int Pontos { get; set; }

        [DefaultValue(0)]
        public int Vitorias { get; set; }

        [DefaultValue(0)]
        public int Empates { get; set; }

        [DefaultValue(0)]
        public int Derrotas { get; set; }

        [DefaultValue(0)]
        public int VitoriasProrrogacao { get; set; }

        [DefaultValue(0)]
        public int VitoriasPenaltis { get; set; }

        [DefaultValue(0)]
        public int DerrotaProrrogacao { get; set; }

        [DefaultValue(0)]
        public int DerrotaPenaltis { get; set; }

        [DefaultValue(0)]
        public int GolsPro { get; set; }

        [DefaultValue(0)]
        public int GolsContra { get; set; }

        [DefaultValue(0)]
        public int SaldoGols { get { return GolsPro - GolsContra; } }

        [DefaultValue(0)]
        public int Publico { get; set; }

        [StringLength(1)]
        public string Local { get; set; }

        [DefaultValue(0)]
        public int Jogos { get; set; }

        [DefaultValue(0)]
        public int Realizado { get; set; }

        [DefaultValue(0)]
        public int EmpateProrrogacao { get; set; }
	}
}