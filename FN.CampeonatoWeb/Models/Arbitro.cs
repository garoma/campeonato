﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("Arbitro")]
    public class Arbitro
    {
        [DisplayName("Código")]
        [Key]        
        public int Id { get; set; }

        [DisplayName("Nome")]
        [StringLength(150)]
        public string Nome { get; set; }

        [DisplayName("Data Nascimento")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Nascimento { get; set; }

        [DisplayName("Tipo Arbitro")]
        public int Tipo { get; set; }

        [DisplayName("Foto")]
        public byte[] Foto { get; set; }

        [NotMapped]
        public string NascimentoF { get { return Nascimento.ToString("dd/MM/yyyy"); } }
    }
}