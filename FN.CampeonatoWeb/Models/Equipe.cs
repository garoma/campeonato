﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("Equipe")]
    public class Equipe
    {
        [DisplayName("Código")]
        [Key]        
        public int Id { get; set; }

        [DisplayName("Descricao")]
        [StringLength(150)]
        public string Descricao { get; set; }

        [DisplayName("Clube")]
        public int ClubeId { get; set; }
        [ForeignKey("ClubeId")]
        public virtual Clube Clube { get; set; }
        public virtual List<Clube> Clubes { get; set; }

        [DisplayName("TipoEsporte")]
        public int TipoEsporteId { get; set; }
        [ForeignKey("TipoEsporteId")]
        public virtual TipoEsporte TipoEsporte { get; set; }
        public virtual List<TipoEsporte> TiposEsportes { get; set; }

        [DisplayName("TipoEquipe")]
        public int TipoEquipeId { get; set; }
        [ForeignKey("TipoEquipeId")]
        public virtual TipoEquipe TipoEquipe { get; set; }
        public virtual List<TipoEquipe> TiposEquipes { get; set; }
    }
}