﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("JogoGeral")]
    public class JogoGeral
    {
        [Key]
        [DisplayName("Código")]
        public int Id { get; set; }

        [DisplayName("Data do Jogo")]
        public DateTime DataJogo { get; set; }

        [DisplayName("Hora do Jogo")]
        [StringLength(5)]
        public string HoraJogo { get; set; }

        [DisplayName("Estadio")]
        public int EstadioId { get; set; }
        [ForeignKey("EstadioId")]
        public virtual Estadio Estadio { get; set; }

        public string Comentarios { get; set; }

        [NotMapped]
        public string DataJogoFormatada { get { return DataJogo.ToString("dd/MM/yyyy"); } }
    }
}