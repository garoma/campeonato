﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("EquipeJogador")]
    public class EquipeJogador
    {
        [DisplayName("Código: ")]
        [Key]
        public int Id { get; set; }

        [DisplayName("Equipe")]
        public int EquipeId { get; set; }
        [ForeignKey("EquipeId")]
        public virtual Equipe Equipe { get; set; }
        public virtual List<Equipe> Equipes { get; set; }

        [DisplayName("Jogador")]
        public int JogadorId { get; set; }
        [ForeignKey("JogadorId")]

        public virtual Jogador Jogador { get; set; }

        public virtual List<Jogador> Jogadores { get; set; }
        [NotMapped]
        public List<VisualizaLista> ListaDados { get; set; }
    }
}