﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("Grupo")]
    public class Grupo
    {
        [Key]
        [DisplayName("Código")]
        public int Id { get; set; }

        [StringLength(100)]
        [DisplayName("Descrição")]
        public string Descricao { get; set; }
    }
}