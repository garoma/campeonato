﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("Campeonato")]
    public class Campeonato
    {
        [DisplayName("Código: ")]
        [Key]
        public int Id { get; set; }

        [DisplayName("Descrição")]
        [StringLength(150)]
        public string Descricao { get; set; }

        [DisplayName("Tipo")]
        public int TipoCampeonatoId { get; set; }
        [ForeignKey("TipoCampeonatoId")]
        public virtual TipoCampeonato TipoCampeonato { get; set; }
        
        [DisplayName("País")]
        [StringLength(30)]
        public string Pais { get; set; }

        public virtual List<TipoCampeonato> TiposCampeonatos { get; set; }
    }
}