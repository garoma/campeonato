﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using FN.CampeonatoWeb.Models;

namespace FN.CampeonatoWeb.Models
{
    public class FNCampeonatoCtx : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public FNCampeonatoCtx() : base("name=FNCampeonatoCtx")
        {
        }

        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Tempo> Tempos { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Evento> Eventos { get; set; }

        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.JogoGeral> JogosGerais { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.JogoArbitro> JogosArbitros { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.JogoEquipe> JogosEquipes { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.JogoEvento> JogosEventos { get; set; }

        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.EquipeCampeonato> EquipesCampeonatos { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.EquipeTemporada> EquipesTemporadas { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.EquipeJogador> EquipesJogadores { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.CampeonatoTemporada> CampeonatosTemporadas { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.ArbitroCampeonato> ArbitrosCampeonatos { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.ArbitroTemporada> ArbitrosTemporadas { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.EstruturaTemporada> EstruturasTemporadas { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Arbitro> Arbitros { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.PosicaoJogador> PosicoesJogadores { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.TipoEsporte> TiposEsportes { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.TipoEquipe> TiposEquipes { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.TipoCampeonato> TiposCampeonatos { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Campeonato> Campeonatos { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Classificacao> Classificacoes { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Clube> Clubes { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Equipe> Equipes { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Estadio> Estadios { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Fase> Fases { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Grupo> Grupos { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Jogador> Jogadores { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Lance> Lances { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Punicao> Punicoes { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Rodada> Rodadas { get; set; }
        public System.Data.Entity.DbSet<FN.CampeonatoWeb.Models.Temporada> Temporadas { get; set; }
        public DbSet<Content> Contents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}
