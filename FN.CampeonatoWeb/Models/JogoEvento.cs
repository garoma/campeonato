﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("JogoEvento")]
    public class JogoEvento
    {
        [Key]
        [DisplayName("Código")]
        public int Id { get; set; }

        [DisplayName("Jogo")]
        public int JogoId { get; set; }
        [ForeignKey("JogoId")]

        public virtual JogoGeral JogoGeral { get; set; }

        [DisplayName("Jogador")]
        public int JogadorId { get; set; }
        [ForeignKey("JogadorId")]
        public virtual Jogador Jogador { get; set; }

        public int EventoId { get; set; }
        [ForeignKey("EventoId")]
        public virtual Evento Evento { get; set; }

        [DisplayName("Tempo")]
        public int TempoId { get; set; }
        [ForeignKey("TempoId")]
        public virtual Tempo Tempo { get; set; }

        [DisplayName("Minuto")]
        [DefaultValue(0)]
        public int Minuto { get; set; }

        [DisplayName("Quantidade")]
        [DefaultValue(0)]
        public int Quantidade { get; set; }
    }
}