﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("ArbitroTemporada")]
    public class ArbitroTemporada
    {
        [DisplayName("Código: ")]
        [Key]
        public int Id { get; set; }

        [DisplayName("Campeonato")]
        public int CampeonatoId { get; set; }
        [ForeignKey("CampeonatoId")]
        public virtual Campeonato Campeonato { get; set; }

        [DisplayName("Temporada")]
        public int TemporadaId { get; set; }
        [ForeignKey("TemporadaId")]
        public virtual Temporada Temporada { get; set; }

        [DisplayName("Arbitro")]
        public int ArbitroId { get; set; }
        [ForeignKey("ArbitroId")]
        public virtual Arbitro Arbitro { get; set; }

        public virtual List<Campeonato> Campeonatos { get; set; }
        public virtual List<Temporada> Temporadas { get; set; }
        public virtual List<Arbitro> Arbitros { get; set; }

        [NotMapped]
        public List<VisualizaLista> ListaDados { get; set; }
    }
}