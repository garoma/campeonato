﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("Estadio")]
    public class Estadio
    {
        [DisplayName("Código")]
        [Key]        
        public int Id { get; set; }

        [DisplayName("Nome")]
        [StringLength(50)]
        public string Nome { get; set; }

        [DisplayName("Descricao")]
        [StringLength(200)]
        public string Descricao { get; set; }

        [DisplayName("Capacidade")]
        public int Capacidade { get; set; }

        [DisplayName("Foto")]
        public byte[] Foto { get; set; }

        [DisplayName("País")]
        [StringLength(50)]
        public string Pais { get; set; }
    }
}