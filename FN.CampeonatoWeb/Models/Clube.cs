﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("Clube")]
    public class Clube
    {
        [DisplayName("Código")]
        [Key]        
        public int Id { get; set; }

        [DisplayName("Nome")]
        [StringLength(30)]
        public string Nome { get; set; }

        [DisplayName("Descricao")]
        [StringLength(150)]
        public string Descricao { get; set; }

        [DisplayName("Data da Fundação")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DataFundacao { get; set; }

        [DisplayName("Cidade")]
        [StringLength(50)]
        public string Cidade { get; set; }

        [DisplayName("País")]
        [StringLength(50)]
        public string Pais { get; set; }

        [DisplayName("Foto")]
        public byte[] Foto { get; set; }

        [DisplayName("Estádio")]
        public int EstadioId { get; set; }
        [ForeignKey("EstadioId")]
        public virtual Estadio Estadio { get; set; }
        public virtual List<Estadio> Estadios { get; set; }
        
        [NotMapped]
        public string DataFundacaoF { get { return DataFundacao.ToString("dd/MM/yyyy"); } }
    }
}