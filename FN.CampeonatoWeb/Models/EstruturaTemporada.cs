﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("EstruturaTemporada")]
    public class EstruturaTemporada
    {
        [DisplayName("Código: ")]
        [Key]
        public int Id { get; set; }

        [DisplayName("Campeonato")]
        public int CampeonatoId { get; set; }
        [ForeignKey("CampeonatoId")]
        public virtual Campeonato Campeonato { get; set; }
        public virtual List<Campeonato> Campeonatos { get; set; }

        [DisplayName("Temporada")]
        public int TemporadaId { get; set; }
        [ForeignKey("TemporadaId")]
        public virtual Temporada Temporada { get; set; }
        public virtual List<Temporada> Temporadas { get; set; }

        [DisplayName("Fase")]
        public int FaseId { get; set; }
        [ForeignKey("FaseId")]
        public virtual Fase Fase { get; set; }
        public virtual List<Fase> Fases { get; set; }

        [DisplayName("Grupo")]
        public int GrupoId { get; set; }
        [ForeignKey("GrupoId")]
        public virtual Grupo Grupo { get; set; }
        public virtual List<Grupo> Grupos { get; set; }

        [DisplayName("Rodada")]
        public int RodadaId { get; set; }
        [ForeignKey("RodadaId")]
        public virtual Rodada Rodada { get; set; }

        public virtual List<Rodada> Rodadas { get; set; }
        [NotMapped]
        public List<VisualizaLista> ListaDados { get; set; }
    }
}