﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("ArbitroCampeonato")]
    public class ArbitroCampeonato
    {
        [DisplayName("Código: ")]
        [Key]
        public int Id { get; set; }

        [DisplayName("Campeonato")]
        public int CampeonatoId { get; set; }
        [ForeignKey("CampeonatoId")]
        public virtual Campeonato Campeonato { get; set; }

        [DisplayName("Arbitro")]
        public int ArbitroId { get; set; }
        [ForeignKey("ArbitroId")]
        public virtual Arbitro Arbitro { get; set; }

        public virtual List<Campeonato> Campeonatos { get; set; }
        public virtual List<Arbitro> Arbitros { get; set; }
        [NotMapped]
        public List<VisualizaLista> ListaDados { get; set; }
    }
}