﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("Punicao")]
    public class Punicao
    {
        [Key]
        [DisplayName("Código")]
        public int Id { get; set; }

        [StringLength(150)]
        [DisplayName("Descrição")]
        public string Descricao { get; set; }

    }
}