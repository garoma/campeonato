﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Models
{
    [Table("JogoEquipe")]
    public class JogoEquipe
    {
        [Key]
        public int Id { get; set; }

        public int JogoId { get; set; }
        [ForeignKey("JogoId")]

        public virtual JogoGeral JogoGeral { get; set; }

        //0 = Casa, 1 - Fora
        public int Tipo { get; set; }

        public int EquipeId { get; set; }
        [ForeignKey("EquipeId")]
        public virtual Equipe Equipe { get; set; }

        public int Gols { get; set; }
    }
}