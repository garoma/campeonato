﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Repositories;
using FN.CampeonatoWeb.Helper;

namespace FN.CampeonatoWeb.Controllers
{
    public class ArbitroCampeonatoController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<ArbitroCampeonato> arbitroCampeonato = new List<ArbitroCampeonato>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Campeonato":
                    arbitroCampeonato = db.ArbitrosCampeonatos.OrderBy(s => s.Campeonato.Descricao).ToList();
                    break;
                case "Arbitro":
                    arbitroCampeonato = db.ArbitrosCampeonatos.OrderBy(s => s.Arbitro.Nome).ToList();
                    break;
                default:
                    arbitroCampeonato = db.ArbitrosCampeonatos.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(arbitroCampeonato.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        [ActionName("Edit")]
        public ActionResult IniciaEdit(ArbitroCampeonato arbitroCampeonato)
        {
            if (arbitroCampeonato.Id == 0)
            {
                ViewBag.Operacao = "I";
            }
            else
            {
                arbitroCampeonato = db.ArbitrosCampeonatos.Find(arbitroCampeonato.Id);
                ViewBag.Operacao = "A";
            }

            CarregaDadosauxiliares(arbitroCampeonato);

            return View(arbitroCampeonato);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(ArbitroCampeonato arbitroCampeonato)
        {
            IsValidacao(arbitroCampeonato);

            if (!ModelState.IsValid)
                return View("Edit", arbitroCampeonato);

            if (arbitroCampeonato.Id == 0)
            {
                db.Entry(arbitroCampeonato).State = EntityState.Added;
                db.ArbitrosCampeonatos.Add(arbitroCampeonato);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit", new ArbitroTemporada { CampeonatoId = arbitroCampeonato.CampeonatoId });
            }
            else
            {
                db.Entry(arbitroCampeonato).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            ArbitroCampeonato arbitroCampeonato = db.ArbitrosCampeonatos.Find(id);
            db.Entry(arbitroCampeonato).State = EntityState.Deleted;
            db.ArbitrosCampeonatos.Remove(arbitroCampeonato);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DropDownlistChange(ArbitroCampeonato arbitroCampeonato)
        {
            return RedirectToAction("Edit", arbitroCampeonato);
        }

        private void CarregaDadosauxiliares(ArbitroCampeonato arbitroCampeonato)
        {
            ViewBag.Campeonatos = db.Campeonatos.Select(d => new SelectListItem { Value = d.Id.ToString(), Text = d.Descricao }).ToList();
            ViewBag.Arbitros = new SelectList(db.Arbitros.ToList(), "Id", "Nome");

            arbitroCampeonato.ListaDados = (from a in db.ArbitrosCampeonatos
                                            join b in db.Campeonatos on a.CampeonatoId equals b.Id
                                            join c in db.Arbitros on a.ArbitroId equals c.Id
                                            where (a.CampeonatoId == arbitroCampeonato.CampeonatoId)
                                            select new VisualizaLista { Campo1 = b.Descricao, Campo2 = c.Nome }).ToList();

            var _tipos = from TipoArbitro d in System.Enum.GetValues(typeof(TipoArbitro))
                         select new { ID = (int)d, Name = d.ToString() };

            ViewData["TipoArbitro"] = new SelectList(_tipos, "ID", "Name");
        }

        public void IsValidacao(ArbitroCampeonato arbitroCampeonato)
        {
            if (arbitroCampeonato.CampeonatoId <= 0)
                ModelState.AddModelError("CampeonatoId", "Selecione um campeonato.");

            if (arbitroCampeonato.ArbitroId <= 0)
                ModelState.AddModelError("ArbitroId", "Selecione um Arbitro.");

            if (db.ArbitrosCampeonatos.Count(x => x.CampeonatoId == arbitroCampeonato.CampeonatoId && x.ArbitroId == arbitroCampeonato.ArbitroId) != 0 && arbitroCampeonato.Id == 0)
                ModelState.AddModelError(string.Empty, "Temporada já associada com o campeonato, gravação não permitida");

            if (arbitroCampeonato.Id != 0)
                if (db.ArbitrosCampeonatos.Any(x => x.CampeonatoId == arbitroCampeonato.CampeonatoId && x.ArbitroId == arbitroCampeonato.ArbitroId))
                    if (db.ArbitrosCampeonatos.Where(x => x.CampeonatoId == arbitroCampeonato.CampeonatoId && x.ArbitroId == arbitroCampeonato.ArbitroId).SingleOrDefault().Id != arbitroCampeonato.Id)
                        ModelState.AddModelError(string.Empty, "Temporada já associada com o campeonato, gravação não permitida");
        }
    }
}
