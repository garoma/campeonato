﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Repositories;
using FN.CampeonatoWeb.Helper;

namespace FN.CampeonatoWeb.Controllers
{
    public class EquipeJogadorController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<EquipeJogador> equipeJogador = new List<EquipeJogador>();
            
            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Equipe":
                    equipeJogador = db.EquipesJogadores.OrderBy(s => s.Equipe.Descricao).ToList();
                    break;
                case "Jogador":
                    equipeJogador = db.EquipesJogadores.OrderBy(s => s.Jogador.Nome).ToList();
                    break;
                default:
                    equipeJogador = db.EquipesJogadores.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(equipeJogador.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        [ActionName("Edit")]
        public ActionResult IniciaEdit(EquipeJogador equipeJogador)
        {
            if (equipeJogador.Id == 0)
            {
                ViewBag.Operacao = "I";
            }
            else
            {
                equipeJogador = db.EquipesJogadores.Find(equipeJogador.Id);
                ViewBag.Operacao = "A";
            }

            CarregaDadosauxiliares(equipeJogador);
            return View(equipeJogador);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(EquipeJogador equipeJogador)
        {
            IsValidacao(equipeJogador);

            if (!ModelState.IsValid)
                return View("Edit", equipeJogador);

            if (equipeJogador.Id == 0)
            {
                db.Entry(equipeJogador).State = EntityState.Added;
                db.EquipesJogadores.Add(equipeJogador);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit", new EquipeJogador { EquipeId = equipeJogador.EquipeId });
            }
            else
            {
                db.Entry(equipeJogador).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            EquipeJogador equipeJogador = db.EquipesJogadores.Find(id);
            db.Entry(equipeJogador).State = EntityState.Deleted;
            db.EquipesJogadores.Remove(equipeJogador);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DropDownlistChange(EquipeJogador equipesJogador)
        {
            return RedirectToAction("Edit", equipesJogador);
        }

        private void CarregaDadosauxiliares(EquipeJogador equipeJogador)
        {
            ViewBag.Equipes = new SelectList(db.Equipes.ToList(), "Id", "Descricao");
            ViewBag.Jogadores = new SelectList(db.Jogadores, "Id", "Nome");

            if (equipeJogador.JogadorId > 0)
            {
                equipeJogador.Jogador = new Jogador { 
                    PosicaoJogador = new PosicaoJogador {
                        Descricao = db.Jogadores.Where(x => x.Id == equipeJogador.JogadorId).SingleOrDefault().PosicaoJogador.Descricao
                    }
                };
            }
                
            equipeJogador.ListaDados = (from a in db.EquipesJogadores
                                        join b in db.Equipes on a.EquipeId equals b.Id
                                        join c in db.Jogadores on a.JogadorId equals c.Id
                                        join d in db.PosicoesJogadores on c.PosicaoJogadorId equals d.Id
                                        where (a.EquipeId == equipeJogador.EquipeId)
                                        select new VisualizaLista { Campo1 = b.Descricao, Campo2 = c.Nome, Campo3 = d.Descricao }).ToList();
        }

        public void IsValidacao(EquipeJogador equipesJogadores)
        {
            if (equipesJogadores.EquipeId <= 0)
                ModelState.AddModelError("EquipeId", "Selecione uma Equipe.");

            if (equipesJogadores.JogadorId <= 0)
                ModelState.AddModelError("JogadorId", "Selecione um jogador.");

            if ((db.EquipesJogadores.Count(x => x.EquipeId == equipesJogadores.EquipeId && x.JogadorId == equipesJogadores.JogadorId) != 0 && equipesJogadores.Id == 0))
                ModelState.AddModelError(string.Empty, "Jogador já associada com o equipe, gravação não permitida");

            if (equipesJogadores.Id != 0)
                if (db.EquipesJogadores.Any(x => x.EquipeId == equipesJogadores.EquipeId && x.JogadorId == equipesJogadores.JogadorId))
                    if (db.EquipesJogadores.Where(x => x.EquipeId == equipesJogadores.EquipeId && x.JogadorId == equipesJogadores.JogadorId).SingleOrDefault().Id != equipesJogadores.Id)
                        ModelState.AddModelError(string.Empty, "Jogador já associada com o equipe, gravação não permitida");
        }
    }
}

