﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;

namespace FN.CampeonatoWeb.Controllers
{
    public class EventoController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<Evento> evento = new List<Evento>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Descricao":
                    evento = db.Eventos.OrderBy(s => s.Descricao).ToList();
                    break;
                default:
                    evento = db.Eventos.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(evento.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        public ActionResult Edit(int? id)
        {
            Evento evento;

            if (id == null || id == 0)
                evento = new Evento();
            else
                evento = db.Eventos.Find(id);

            return View(evento);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(Evento evento)
        {
            IsValidacao(evento);

            if (!ModelState.IsValid)
                return View("Edit", evento);

            if (evento.Id == 0)
            {
                db.Entry(evento).State = EntityState.Added;
                db.Eventos.Add(evento);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit");
            }
            else
            {
                db.Entry(evento).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            Evento evento = db.Eventos.Find(id);
            db.Entry(evento).State = EntityState.Deleted;
            db.Eventos.Remove(evento);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }

        public void IsValidacao(Evento evento)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(evento.Descricao))
                ModelState.AddModelError("Descricao", "Campo de preenchimento obrigatório.");
        }
    }
}
