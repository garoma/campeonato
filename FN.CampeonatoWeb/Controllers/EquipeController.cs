﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Repositories;
using FN.CampeonatoWeb.Helper;

namespace FN.CampeonatoWeb.Controllers
{
    public class EquipeController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;

            List<Equipe> equipe = new List<Equipe>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Descricao":
                    equipe = db.Equipes.OrderBy(s => s.Descricao).ToList();
                    break;
                case "Clube":
                    equipe = db.Equipes.OrderBy(s => s.Clube.Descricao).ToList();
                    break;
                case "TipoEsporte":
                    equipe = db.Equipes.OrderBy(s => s.TipoEsporte.Descricao).ToList();
                    break;
                case "TipoEquipe":
                    equipe = db.Equipes.OrderBy(s => s.TipoEquipe.Descricao).ToList();
                    break;
                default:
                    equipe = db.Equipes.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(equipe.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        public ActionResult Edit(int? id)
        {
            Equipe equipe;

            if (id == null || id == 0)
                equipe = new Equipe();
            else
                equipe = db.Equipes.Find(id);

            ViewBag.Clubes = db.Clubes.Select(d => new SelectListItem { Value = d.Id.ToString(), Text = d.Descricao }).ToList();
            ViewBag.TiposEsportes = db.TiposEsportes.Select(d => new SelectListItem { Value = d.Id.ToString(), Text = d.Descricao }).ToList();
            ViewBag.TiposEquipes = db.TiposEquipes.Select(d => new SelectListItem { Value = d.Id.ToString(), Text = d.Descricao }).ToList();
            ViewBag.Estadios = db.Estadios.Select(d => new SelectListItem { Value = d.Id.ToString(), Text = d.Descricao }).ToList();

            return View(equipe);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(Equipe equipe)
        {
            IsValidacao(equipe);

            if (!ModelState.IsValid)
                return View("Edit", equipe);

            ViewBag.Clubes = db.Clubes.Select(d => new SelectListItem { Value = d.Id.ToString(), Text = d.Descricao }).ToList();
            ViewBag.TiposEsportes = db.TiposEsportes.Select(d => new SelectListItem { Value = d.Id.ToString(), Text = d.Descricao }).ToList();
            ViewBag.TiposEquipes = db.TiposEquipes.Select(d => new SelectListItem { Value = d.Id.ToString(), Text = d.Descricao }).ToList();
            ViewBag.Estadios = db.Estadios.Select(d => new SelectListItem { Value = d.Id.ToString(), Text = d.Descricao }).ToList();
            
            if (equipe.Id == 0)
            {
                db.Entry(equipe).State = EntityState.Added;
                db.Equipes.Add(equipe);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit");
            }
            else
            {
                db.Entry(equipe).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            Equipe equipe = db.Equipes.Find(id);
            db.Entry(equipe).State = EntityState.Deleted;
            db.Equipes.Remove(equipe);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }

        public void IsValidacao(Equipe equipe)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(equipe.Descricao))
                ModelState.AddModelError("Descricao", "Campo de preenchimento obrigatório.");

            if (equipe.ClubeId <= 0)
                ModelState.AddModelError("ClubeId", "Selecione um Clube.");

            if (equipe.TipoEsporteId <= 0)
                ModelState.AddModelError("TipoEsporteId", "Selecione im Tipo de Esporte.");

            if (equipe.TipoEquipeId <= 0)
                ModelState.AddModelError("TipoEquipeId", "Selecione um aEquipe.");
        }
    }
}
