﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Repositories;
using FN.CampeonatoWeb.Helper;
using FN.CampeonatoWeb.ViewModel;

namespace FN.CampeonatoWeb.Controllers
{
    public class ClassificacaoController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();




//Fase
//Grupo




//Posicao
//Equipe
//(Equipes de Fora ou de Casa ou todas)


//PTS = Pontos
//J = Jogos
//V = Votorias
//E = Empates
//D = Derrotas
//GP (GOLS PRO)= Ataques
//GC (GOLS CONTRA)= Defesas
//SG = Saldo de Gols
//% = Aproveitamento

        public ActionResult Filtro()
        {
            CarregaDadosauxiliares();
            return View();
        }

        //public ActionResult List(string sortOrder, int? Page_No)
        //{
        //    ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
        //    ViewBag.Posicoes = db.Estadios.Select(d => new SelectListItem { Value = d.Id.ToString(), Text = d.Nome }).ToList();

        //    List<Classificacao> Classificacao = new List<Classificacao>();

        //    switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
        //    {
        //        case "Nome":
        //            Classificacao = db.Classificacaos.OrderBy(s => s.Nome).ToList();
        //            break;
        //        case "Descricao":
        //            Classificacao = db.Classificacaos.OrderBy(s => s.Descricao).ToList();
        //            break;
        //        case "DataFundacao":
        //            Classificacao = db.Classificacaos.OrderBy(s => s.DataFundacao).ToList();
        //            break;
        //        case "Estadio":
        //            Classificacao = db.Classificacaos.OrderBy(s => s.Estadio).ToList();
        //            break;
        //        case "Cidade":
        //            Classificacao = db.Classificacaos.OrderBy(s => s.Cidade).ToList();
        //            break;
        //        case "Pais":
        //            Classificacao = db.Classificacaos.OrderBy(s => s.Pais).ToList();
        //            break;
        //        default:
        //            Classificacao = db.Classificacaos.OrderBy(s => s.Id).ToList();
        //            break;
        //    }
        //    int Size_Of_Page = 20;
        //    int No_Of_Page = (Page_No ?? 1);

        //    CarregaDadosauxiliares(ClassificacaoViewModel classificacao)

        //    return View(Classificacao.ToPagedList(No_Of_Page, Size_Of_Page));
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult List(int? id)
        //{
        //    Classificacao Classificacao;

        //    if (id == null || id == 0)
        //        Classificacao = new Classificacao();
        //    else
        //        Classificacao = db.Classificacaos.Find(id);

        //    ViewBag.Estadios = db.Estadios.Select(d => new SelectListItem { Value = d.Id.ToString(), Text = d.Descricao }).ToList();
        //    return View(Classificacao);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DropDownlistChange(ClassificacaoViewModel classificacao)
        {
            return RedirectToAction("Filtro", classificacao);
        }

        private void CarregaDadosauxiliares()
        {
            ViewBag.Temporadas = new SelectList(db.Temporadas.ToList(), "Id", "Descricao");
            ViewBag.Fases = new SelectList(db.Fases.ToList(), "Id", "Descricao");
            ViewBag.Grupos = new SelectList(db.Grupos.ToList(), "Id", "Descricao");
        }

        public ActionResult RetrieveClubeImage(int id)
        {
            try
            {
                var q = from temp in db.Clubes where temp.Id == id select temp.Foto;
                byte[] cover = q.First();
                return File(cover, "image/jpg");
            }
            catch
            {
                return null;
            }
        }
    }
}
