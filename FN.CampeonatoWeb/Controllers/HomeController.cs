﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FN.CampeonatoWeb.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Liga esportiva de Guaianases. Estamos a mais de 20 anos trabalhando com dedicação e amor a nossa atividade";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contate-nos por e-mail e telefone.";
            return View();
        }

        public ActionResult Inicio()
        {
            ViewBag.Message = "Página inicial.";
            return View();
        }
    }
}