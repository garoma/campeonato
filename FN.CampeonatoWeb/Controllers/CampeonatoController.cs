﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;

namespace FN.CampeonatoWeb.Controllers
{
    public class CampeonatoController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<Campeonato> campeonato = new List<Campeonato>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Descricao":
                    campeonato = db.Campeonatos.OrderBy(s => s.Descricao).ToList();
                    break;
                case "TipoCampeonato":
                    campeonato = db.Campeonatos.OrderBy(s => s.TipoCampeonato.Descricao).ToList();
                    break;
                case "Pais":
                    campeonato = db.Campeonatos.OrderBy(s => s.Pais).ToList();
                    break;
                default:
                    campeonato = db.Campeonatos.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(campeonato.ToPagedList(No_Of_Page, Size_Of_Page));
        }
        public ActionResult Edit(int? id)
        {
            Campeonato campeonato;

            if (id == null || id == 0)
                campeonato = new Campeonato();
            else
                campeonato = db.Campeonatos.Find(id);

            ViewBag.TiposCampeonatos = db.TiposCampeonatos.Select(d => new SelectListItem { Value = d.Id.ToString(), Text = d.Descricao }).ToList();
            return View(campeonato);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(Campeonato campeonato)
        {
            IsValidacao(campeonato);

            if (!ModelState.IsValid)
                return View("Edit", campeonato);

            if (campeonato.Id == 0)
            {
                db.Entry(campeonato).State = EntityState.Added;
                db.Campeonatos.Add(campeonato);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit");
            }
            else
            {
                db.Entry(campeonato).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            Campeonato campeonato = db.Campeonatos.Find(id);
            db.Entry(campeonato).State = EntityState.Deleted;
            db.Campeonatos.Remove(campeonato);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }

        public void IsValidacao(Campeonato campeonato)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(campeonato.Descricao))
                ModelState.AddModelError("Descricao", "Campo de preenchimento obrigatório.");

            if (campeonato.TipoCampeonatoId <= 0)
                ModelState.AddModelError("TipoCampeonatoId", "Selecione um Campeonato.");

            if (string.IsNullOrEmpty(campeonato.Pais))
                ModelState.AddModelError("Pais", "Campo de preenchimento obrigatório.");
        }
    }
}
