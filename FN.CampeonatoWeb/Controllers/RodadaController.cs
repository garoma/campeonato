﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;

namespace FN.CampeonatoWeb.Controllers
{
    public class RodadaController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<Rodada> rodada = new List<Rodada>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Descricao":
                    rodada = db.Rodadas.OrderBy(s => s.Descricao).ToList();
                    break;
                default:
                    rodada = db.Rodadas.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(rodada.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        public ActionResult Edit(int? id)
        {
            Rodada rodada;

            if (id == null || id == 0)
                rodada = new Rodada();
            else
                rodada = db.Rodadas.Find(id);

            return View(rodada);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(Rodada rodada)
        {
            IsValidacao(rodada);

            if (!ModelState.IsValid)
                return View("Edit", rodada);

            if (rodada.Id == 0)
            {
                db.Entry(rodada).State = EntityState.Added;
                db.Rodadas.Add(rodada);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit");
            }
            else
            {
                db.Entry(rodada).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            Rodada rodada = db.Rodadas.Find(id);
            db.Entry(rodada).State = EntityState.Deleted;
            db.Rodadas.Remove(rodada);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }
        
        public void IsValidacao(Rodada rodada)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(rodada.Descricao))
                ModelState.AddModelError("Descricao", "Campo de preenchimento obrigatório.");
        }
    }
}
