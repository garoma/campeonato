﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Repositories;
using FN.CampeonatoWeb.Helper;
using FN.CampeonatoWeb.ViewModel;

namespace FN.CampeonatoWeb.Controllers
{
    public class JogoCasaEventoController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        [ActionName("Edit")]
        public ActionResult IniciaEdit(int id, int? idEvento)
        {
            JogoViewModel jogoViewModel = CarregaEventoCasa(id, idEvento);
            CarregaDadosAuxiliares(jogoViewModel);
            return View("Edit", jogoViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DropDownlistChange(JogoViewModel jogoViewModel)
        {
            jogoViewModel = CarregaEventoCasa(jogoViewModel.JogoId, null);
            CarregaDadosAuxiliares(jogoViewModel);
            return View("Edit", jogoViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "IncluirEvento")]
        public ActionResult IncluirEvento(JogoViewModel jogoViewModel)
        {
            IsValidacao(jogoViewModel);
            jogoViewModel.AlteraEventoCasaId = 0;

            JogoEvento eventoCasa = new JogoEvento
            {
                JogoId = jogoViewModel.JogoId,
                JogadorId = jogoViewModel.JogadorCasaId,
                EventoId = jogoViewModel.EventoCasaId,
                TempoId = jogoViewModel.TempoCasaId,
                Minuto = jogoViewModel.MinutoCasa,
                Quantidade = jogoViewModel.QuantidadeCasa
            };

            if (jogoViewModel.AlteraEventoCasaId == 0)
            {
                db.Entry(eventoCasa).State = EntityState.Added;
                db.JogosEventos.Add(eventoCasa);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
            }
            else
            {
                db.Entry(eventoCasa).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
            }
            CarregaDadosAuxiliares(jogoViewModel);
            return RedirectToAction("Edit", "JogoCasaEvento", new { id = jogoViewModel.JogoId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "ExcluirEvento")]
        public ActionResult ExcluirEvento(int idJogoEvento)
        {
            JogoEvento jogo = new JogoEvento();
            
            jogo = db.JogosEventos.Find(idJogoEvento);
            db.Entry(jogo).State = EntityState.Deleted;
            db.JogosEventos.Remove(jogo);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("Edit", "JogoCasaEvento", new { id = jogo.JogoId });
        }

        private void CarregaDadosAuxiliares(JogoViewModel jogoViewModel)
        {
            ViewBag.Jogadores = new SelectList(db.Jogadores.ToList(), "Id", "Nome");
            ViewBag.Eventos = new SelectList(db.Eventos.ToList(), "Id", "Descricao");
            ViewBag.Tempos = new SelectList(db.Tempos.ToList(), "Id", "Descricao");
        }

        private JogoViewModel CarregaEventoCasa(int id, int? idEvento)
        {
            JogoViewModel jogoViewModel = new JogoViewModel();
            List<JogoEvento> jogo = new List<JogoEvento>();

            jogo = db.JogosEventos.Where(x => x.JogoId == id).ToList();
            jogoViewModel.JogoId = id;
            jogoViewModel.AlteraEventoCasaId = idEvento != null ? idEvento.Value : 0;

            if (jogo.Count <= 0)
                return jogoViewModel;

            jogoViewModel.EventosCasa = new List<JogoEvento>();

            foreach (JogoEvento item in jogo)
            {
                if (jogoViewModel.AlteraEventoCasaId == item.EventoId)
                {
                    jogoViewModel.JogadorCasaId = item.JogadorId;
                    jogoViewModel.EventoCasaId = item.EventoId;
                    jogoViewModel.TempoCasaId = item.TempoId;
                    jogoViewModel.MinutoCasa = item.Minuto;
                    jogoViewModel.QuantidadeCasa = item.Quantidade;
                }

                jogoViewModel.EventosCasa.Add(
                    new JogoEvento
                    {
                        Id = item.Id,
                        JogoId = id,
                        JogadorId = item.JogadorId,
                        EventoId = item.EventoId,
                        TempoId = item.TempoId,
                        Minuto = item.TempoId,
                        Quantidade = item.Quantidade,
                        Jogador = new Jogador { Nome = db.Jogadores.SingleOrDefault(x => x.Id == item.JogadorId).Nome },
                        Evento = new Evento { Descricao = db.Eventos.SingleOrDefault(x => x.Id == item.EventoId).Descricao },
                        Tempo = new Tempo { Descricao = db.Tempos.SingleOrDefault(x => x.Id == item.TempoId).Descricao }
                    });
            }
            return jogoViewModel;
        }
        public void IsValidacao(JogoViewModel jogoViewModel)
        {
            if (jogoViewModel.JogadorCasaId == 0)
                ModelState.AddModelError("JogadorCasaId", "Campo de preenchimento obrigatorio.");

            if (jogoViewModel.EventoCasaId == 0)
                ModelState.AddModelError("EventoCasaId", "Campo de preenchimento obrigatorio.");

            if (jogoViewModel.TempoCasaId == 0)
                ModelState.AddModelError("TempoCasaId", "Campo de preenchimento obrigatorio.");

            if (jogoViewModel.MinutoCasa <= 0)
                ModelState.AddModelError("MinutoCasa", "Campo de preenchimento obrigatorio.");

            if (jogoViewModel.QuantidadeCasa <= 0)
                ModelState.AddModelError("QuantidadeCasa", "Campo de preenchimento obrigatorio.");
        }
        public ActionResult RetrieveJogadorImage(int id)
        {
            try
            {
                var q = from temp in db.Jogadores where temp.Id == id select temp.Foto;
                byte[] cover = q.First();
                return File(cover, "image/jpg");
            }
            catch
            {
                return null;
            }
        }
    }
}
