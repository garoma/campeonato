﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;

namespace FN.CampeonatoWeb.Controllers
{
    public class TipoEsporteController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<TipoEsporte> tipoEsporte = new List<TipoEsporte>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Descricao":
                    tipoEsporte = db.TiposEsportes.OrderBy(s => s.Descricao).ToList();
                    break;
                default:
                    tipoEsporte = db.TiposEsportes.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(tipoEsporte.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        public ActionResult Edit(int? id)
        {
            TipoEsporte tipoEsporte;

            if (id == null || id == 0)
                tipoEsporte = new TipoEsporte();
            else
                tipoEsporte = db.TiposEsportes.Find(id);

            return View(tipoEsporte);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(TipoEsporte tipoEsporte)
        {
            IsValidacao(tipoEsporte);

            if (!ModelState.IsValid)
                return View("Edit", tipoEsporte);

            if (tipoEsporte.Id == 0)
            {
                db.Entry(tipoEsporte).State = EntityState.Added;
                db.TiposEsportes.Add(tipoEsporte);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit");
            }
            else
            {
                db.Entry(tipoEsporte).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            TipoEsporte tipoEsporte = db.TiposEsportes.Find(id);
            db.Entry(tipoEsporte).State = EntityState.Deleted;
            db.TiposEsportes.Remove(tipoEsporte);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }

        public void IsValidacao(TipoEsporte tipoEsporte)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(tipoEsporte.Descricao))  
                 ModelState.AddModelError("Descricao","Campo de preenchimento obrigatório.");
        }
    }
}
