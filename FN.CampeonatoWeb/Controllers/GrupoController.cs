﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;

namespace FN.CampeonatoWeb.Controllers
{
    public class GrupoController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<Grupo> grupo = new List<Grupo>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Descricao":
                    grupo = db.Grupos.OrderBy(s => s.Descricao).ToList();
                    break;
                default:
                    grupo = db.Grupos.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(grupo.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        public ActionResult Edit(int? id)
        {
            Grupo grupo;

            if (id == null || id == 0)
                grupo = new Grupo();
            else
                grupo = db.Grupos.Find(id);

            return View(grupo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(Grupo grupo)
        {
            IsValidacao(grupo);

            if (!ModelState.IsValid)
                return View("Edit", grupo);

            if (grupo.Id == 0)
            {
                db.Entry(grupo).State = EntityState.Added;
                db.Grupos.Add(grupo);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit");
            }
            else
            {
                db.Entry(grupo).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            Grupo grupo = db.Grupos.Find(id);
            db.Entry(grupo).State = EntityState.Deleted;
            db.Grupos.Remove(grupo);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }

        public void IsValidacao(Grupo grupo)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(grupo.Descricao))
                ModelState.AddModelError("Descricao", "Campo de preenchimento obrigatório.");
        }

    }
}
