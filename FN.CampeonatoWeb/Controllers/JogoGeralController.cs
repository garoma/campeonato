﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Repositories;
using FN.CampeonatoWeb.Helper;
using FN.CampeonatoWeb.ViewModel;
using System.Globalization;
using System.Threading;

namespace FN.CampeonatoWeb.Controllers
{
    public class JogoGeralController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<JogoGeral> jogosViewModel = new List<JogoGeral>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Data":
                    jogosViewModel = db.JogosGerais.OrderBy(s => s.DataJogo).ToList();
                    break;
                default:
                    jogosViewModel = db.JogosGerais.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(jogosViewModel.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        [ActionName("Edit")]
        public ActionResult IniciaEdit(int? id, int? tipo)
        {
            JogoViewModel jogoViewModel = new JogoViewModel();

            if (id == null)
                ViewBag.Operacao = "I";
            else
            {
                JogoGeral jogo = new JogoGeral();
                jogo = db.JogosGerais.Find(id);
                jogoViewModel.JogoId = jogo.Id;
                jogoViewModel.Data = jogo.DataJogo.ToString("yyyy-MM-dd");
                jogoViewModel.Hora = jogo.HoraJogo;

                //if (tipo != null)
                //    if (tipo == 0)
                //        jogoViewModel.AlteraEventoCasaId = 0;
                //    else
                //        jogoViewModel.AlteraEventoForaaId = 0;

                jogoViewModel.EstadioId = jogo.EstadioId;

                if (jogoViewModel.ArbitrosSelecionados == null)
                    jogoViewModel.ArbitrosSelecionados = new List<int>();

                List<JogoArbitro> arbitros = new List<JogoArbitro>();
                arbitros = db.JogosArbitros.Where(x => x.JogoId == jogoViewModel.JogoId).ToList();
                arbitros.ForEach(x=> jogoViewModel.ArbitrosSelecionados.Add(x.ArbitroId));

                jogoViewModel.JogoId = jogo.Id;
                jogoViewModel.Data = jogo.DataJogo.ToString("yyyy-MM-dd"); 
                jogoViewModel.Hora = jogo.HoraJogo;
                jogoViewModel.EstadioId = jogo.EstadioId;
                jogoViewModel.Comentarios = jogo.Comentarios;

                jogoViewModel.Comentarios = jogo.Comentarios;
                ViewBag.Operacao = "A";
            }

            CarregaDadosAuxiliares(jogoViewModel);
            return View("Edit", jogoViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DropDownlistChange(JogoViewModel jogoViewModel)
        {
            CarregaDadosAuxiliares(jogoViewModel);
            return View("Edit", jogoViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "SaveGeralCasa")]
        public ActionResult SaveGeralCasa(JogoViewModel jogoViewModel)
        {
            Salvar(jogoViewModel);
            return RedirectToAction("Edit", "JogoCasa", new { id = jogoViewModel.JogoId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "SaveGeralFora")]
        public ActionResult SaveGeralFora(JogoViewModel jogoViewModel)
        {
            Salvar(jogoViewModel);
            return RedirectToAction("Edit", "JogoFora", new { id = jogoViewModel.JogoId });
        }

        public ActionResult Excluir(int id)
        {
            List<JogoArbitro> arbitros = db.JogosArbitros.Where(x=> x.Id == id).ToList();
            foreach(JogoArbitro apaga in arbitros)
            {
                db.Entry(apaga).State = EntityState.Deleted;
                db.JogosArbitros.Remove(apaga);
                db.SaveChanges();
            }

            List<JogoEquipe> equipes = db.JogosEquipes.Where(x=> x.Id == id).ToList();
            foreach(JogoEquipe apaga in equipes)
            {
                db.Entry(apaga).State = EntityState.Deleted;
                db.JogosEquipes.Remove(apaga);
                db.SaveChanges();
            }
            
            List<JogoEvento> eventos = db.JogosEventos.Where(x=> x.Id == id).ToList();
            foreach(JogoEvento apaga in eventos)
            {
                db.Entry(apaga).State = EntityState.Deleted;
                db.JogosEventos.Remove(apaga);
                db.SaveChanges();
            }

            JogoGeral geral = db.JogosGerais.Find(id);
            db.Entry(geral).State = EntityState.Deleted;
            db.JogosGerais.Remove(geral);
            db.SaveChanges();
            
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }

        private void CarregaDadosAuxiliares(JogoViewModel jogoViewModel)
        {
            ViewBag.Estadios = new SelectList(db.Estadios.ToList(), "Id", "Descricao");
            ViewBag.Arbitros = new SelectList(db.Arbitros.ToList(), "Id", "Nome");
        }

        public void IsValidacao(JogoViewModel jogoViewModel)
        {
            //if (jogoViewModel.Data == DateTime.MinValue)
            //    ModelState.AddModelError("Data", "Campo de preenchimento obrigatorio.");

            if (string.IsNullOrEmpty(jogoViewModel.Hora))
                ModelState.AddModelError("Hora", "Campo de preenchimento obrigatorio.");

            if (jogoViewModel.EstadioId == 0)
                ModelState.AddModelError("EstadioId", "Campo de preenchimento obrigatorio.");

            if (jogoViewModel.ArbitrosSelecionados.Count <= 0)
                ModelState.AddModelError("ArbitrosSelecionados", "Campo de preenchimento obrigatorio.");

            if (string.IsNullOrEmpty(jogoViewModel.Comentarios))
                ModelState.AddModelError("Comentarios", "Campo de preenchimento obrigatorio.");
        }
        private void Salvar(JogoViewModel jogoViewModel)
        {
            JogoGeral jogo;
            
            if (jogoViewModel.JogoId == 0)
            {
                jogo = new JogoGeral();
                jogo.DataJogo = DateTime.Parse(jogoViewModel.Data);
                jogo.HoraJogo = jogoViewModel.Hora;
                jogo.EstadioId = jogoViewModel.EstadioId;
                jogo.Comentarios = jogoViewModel.Comentarios;

                db.Entry(jogo).State = EntityState.Added;
                db.JogosGerais.Add(jogo);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                jogoViewModel.JogoId = jogo.Id;
            }
            else
            {
                jogo = db.JogosGerais.Find(jogoViewModel.JogoId);
                
                db.Entry(jogo).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
            }
            SalvarArbitros(jogoViewModel);
            return; 
        }
        private void SalvarArbitros(JogoViewModel jogoViewModel)
        {
            for(int x = 0; x <= jogoViewModel.ArbitrosSelecionados.Count - 1; x++)
            {
                int arbitroId = jogoViewModel.ArbitrosSelecionados[x];
                var jogo = db.JogosArbitros.SingleOrDefault(y => y.JogoId == jogoViewModel.JogoId && y.ArbitroId == arbitroId);

                if (jogo == null)
                {
                    jogo = new JogoArbitro();
                    jogo.JogoId = jogoViewModel.JogoId;
                    jogo.ArbitroId = jogoViewModel.ArbitrosSelecionados[x];

                    db.Entry(jogo).State = EntityState.Added;
                    db.JogosArbitros.Add(jogo);
                    db.SaveChanges();
                }
                else
                {
                    db.Entry(jogo).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            return;
        }
        public ActionResult RetrieveEstadioImage(int id)
        {
            try
            {
                var q = from temp in db.Estadios where temp.Id == id select temp.Foto;
                byte[] cover = q.First();
                return File(cover, "image/jpg");
            }
            catch
            {
                return null;
            }
        }
    }
}
