﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Repositories;
using FN.CampeonatoWeb.Helper;

namespace FN.CampeonatoWeb.Controllers
{
    public class CampeonatoTemporadaController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<CampeonatoTemporada> campeonatoTemporada = new List<CampeonatoTemporada>();
            
            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Campeonato":
                    campeonatoTemporada = db.CampeonatosTemporadas.OrderBy(s => s.Campeonato.Descricao).ToList();
                    break;
                case "Temporada":
                    campeonatoTemporada = db.CampeonatosTemporadas.OrderBy(s => s.Temporada.Descricao).ToList();
                    break;
                default:
                    campeonatoTemporada = db.CampeonatosTemporadas.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(campeonatoTemporada.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        [ActionName("Edit")]
        public ActionResult IniciaEdit(CampeonatoTemporada campeonatoTemporada)
        {
            if (campeonatoTemporada.Id == 0)
            {
                ViewBag.Operacao = "I";
            }
            else
            {
                campeonatoTemporada = db.CampeonatosTemporadas.Find(campeonatoTemporada.Id);
                ViewBag.Operacao = "A";
            }

            CarregaDadosAuxiliar(campeonatoTemporada);

            return View(campeonatoTemporada);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(CampeonatoTemporada campeonatoTemporada)
        {
            IsValidacao(campeonatoTemporada);

            if (!ModelState.IsValid)
                return View("Edit", campeonatoTemporada);

            if (campeonatoTemporada.Id == 0)
            {
                db.Entry(campeonatoTemporada).State = EntityState.Added;
                db.CampeonatosTemporadas.Add(campeonatoTemporada);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit", new CampeonatoTemporada { CampeonatoId = campeonatoTemporada.CampeonatoId });
            }
            else
            {
                db.Entry(campeonatoTemporada).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            CampeonatoTemporada campeonatoTemporada = db.CampeonatosTemporadas.Find(id);
            db.Entry(campeonatoTemporada).State = EntityState.Deleted;
            db.CampeonatosTemporadas.Remove(campeonatoTemporada);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DropDownlistChange(CampeonatoTemporada campeonatoTemporada)
        {
            return RedirectToAction("Edit", campeonatoTemporada);
        }

        private void CarregaDadosAuxiliar(CampeonatoTemporada campeonatoTemporada)
        {
            ViewBag.Campeonatos = new SelectList(db.Campeonatos.ToList(), "Id", "Descricao");
            ViewBag.Temporadas = new SelectList(db.Temporadas.ToList(), "Id", "Descricao");

            campeonatoTemporada.ListaDados = (from a in db.CampeonatosTemporadas
                                              join b in db.Campeonatos on a.CampeonatoId equals b.Id
                                              join c in db.Temporadas on a.TemporadaId equals c.Id
                                              where (a.CampeonatoId == campeonatoTemporada.CampeonatoId)
                                              select new VisualizaLista { Campo1 = b.Descricao, Campo2 = c.Descricao }).ToList();
        }
        
        public void IsValidacao(CampeonatoTemporada campeonatoTemporada)
        {
            if (campeonatoTemporada.CampeonatoId <= 0)
                ModelState.AddModelError("CampeonatoId", "Selecione um Campeonato.");

            if (campeonatoTemporada.TemporadaId <= 0)
                ModelState.AddModelError("TemporadaId", "Selecione uma Temporada.");

            if ((db.CampeonatosTemporadas.Count(x => x.CampeonatoId == campeonatoTemporada.CampeonatoId && x.TemporadaId == campeonatoTemporada.TemporadaId) != 0 && campeonatoTemporada.Id == 0))
                ModelState.AddModelError(string.Empty, "Temporada já associada com o campeonato, gravação não permitida");

            if (campeonatoTemporada.Id != 0)
                if (db.CampeonatosTemporadas.Any(x => x.CampeonatoId == campeonatoTemporada.CampeonatoId && x.TemporadaId == campeonatoTemporada.TemporadaId))
                    if (db.CampeonatosTemporadas.Where(x => x.CampeonatoId == campeonatoTemporada.CampeonatoId && x.TemporadaId == campeonatoTemporada.TemporadaId).SingleOrDefault().Id != campeonatoTemporada.Id)
                        ModelState.AddModelError(string.Empty, "Temporada já associada com o campeonato, gravação não permitida");
        }
    }
}
