﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Repositories;
using FN.CampeonatoWeb.Helper;

namespace FN.CampeonatoWeb.Controllers
{
    public class ArbitroTemporadaController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<ArbitroTemporada> arbitroTemporada = new List<ArbitroTemporada>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Campeonato":
                    arbitroTemporada = db.ArbitrosTemporadas.OrderBy(s => s.Campeonato.Descricao).ToList();
                    break;
                case "Temporada":
                    arbitroTemporada = db.ArbitrosTemporadas.OrderBy(s => s.Temporada.Descricao).ToList();
                    break;
                case "Arbitro":
                    arbitroTemporada = db.ArbitrosTemporadas.OrderBy(s => s.Arbitro.Nome).ToList();
                    break;
                default:
                    arbitroTemporada = db.ArbitrosTemporadas.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(arbitroTemporada.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        [ActionName("Edit")]
        public ActionResult IniciaEdit(ArbitroTemporada arbitroTemporada)
        {
            if (arbitroTemporada.Id == 0)
            {
                ViewBag.Operacao = "I";
            }
            else
            {
                arbitroTemporada = db.ArbitrosTemporadas.Find(arbitroTemporada.Id);
                ViewBag.Operacao = "A";
            }

            CarregaDadosauxiliares(arbitroTemporada);
            return View(arbitroTemporada);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(ArbitroTemporada arbitroTemporada)
        {
            IsValidacao(arbitroTemporada);

            if (!ModelState.IsValid)
                return View("Edit", arbitroTemporada);

            if (arbitroTemporada.Id == 0)
            {
                db.Entry(arbitroTemporada).State = EntityState.Added;
                db.ArbitrosTemporadas.Add(arbitroTemporada);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit", new ArbitroTemporada { CampeonatoId = arbitroTemporada.CampeonatoId, TemporadaId = arbitroTemporada.TemporadaId });
            }
            else
            {
                db.Entry(arbitroTemporada).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DropDownlistChange(ArbitroTemporada arbitroTemporada)
        {
            return RedirectToAction("Edit", arbitroTemporada);
        }

        public ActionResult Excluir(int id)
        {
            ArbitroTemporada arbitroTemporada = db.ArbitrosTemporadas.Find(id);
            db.Entry(arbitroTemporada).State = EntityState.Deleted;
            db.ArbitrosTemporadas.Remove(arbitroTemporada);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }

        private void CarregaDadosauxiliares(ArbitroTemporada arbitroTemporada)
        {
            ViewBag.Campeonatos = new SelectList(db.Campeonatos.ToList(), "Id", "Descricao");

            if (arbitroTemporada.CampeonatoId != 0)
            {
                var lista = (from a in db.CampeonatosTemporadas
                             join b in db.Temporadas on a.TemporadaId equals b.Id
                             where (a.CampeonatoId == arbitroTemporada.CampeonatoId)
                             select new SelectListItem { Value = b.Id.ToString(), Text = b.Descricao }).ToList();

                //ViewBag.Temporadas = new SelectList(lista, "Id", "Descricao");
                ViewBag.Temporadas = lista;
            }
            else
            {
                ViewBag.Temporadas = new List<SelectListItem>();
            }

            ViewBag.Arbitros = new SelectList(db.Arbitros.ToList(), "Id", "Nome");

            arbitroTemporada.ListaDados = ((from a in db.ArbitrosTemporadas
                                            join b in db.Campeonatos on a.CampeonatoId equals b.Id
                                            join c in db.Temporadas on a.TemporadaId equals c.Id
                                            join d in db.Arbitros on a.ArbitroId equals d.Id
                                            where (a.CampeonatoId == arbitroTemporada.CampeonatoId) &&
                                            (a.TemporadaId == arbitroTemporada.TemporadaId || arbitroTemporada.TemporadaId == 0)
                                            select new VisualizaLista { Campo1 = b.Descricao, Campo2 = c.Descricao, Campo3 = d.Nome }).ToList());

            var _tipos = from TipoArbitro d in System.Enum.GetValues(typeof(TipoArbitro))
                         select new { ID = (int)d, Name = d.ToString() };
            ViewData["TipoArbitro"] = new SelectList(_tipos, "ID", "Name");
        }

        public void IsValidacao(ArbitroTemporada arbitroTemporada)
        {
            if (arbitroTemporada.CampeonatoId <= 0)
                ModelState.AddModelError("CampeonatoId", "Selecione um Campeonato.");

            if (arbitroTemporada.TemporadaId <= 0)
                ModelState.AddModelError("TemporadaId", "Selecione uma Temporada.");

            if (arbitroTemporada.ArbitroId <= 0)
                ModelState.AddModelError("ArbitroId", "Selecione um Arbitro.");

            if (db.CampeonatosTemporadas.Count(x => x.CampeonatoId == arbitroTemporada.CampeonatoId && x.TemporadaId == arbitroTemporada.TemporadaId) == 0)
                ModelState.AddModelError(string.Empty, "Temporada não associada com este campeonato, gravação não permitida");

            if (db.ArbitrosTemporadas.Count(x => x.CampeonatoId == arbitroTemporada.CampeonatoId && x.TemporadaId == arbitroTemporada.TemporadaId && x.ArbitroId == arbitroTemporada.ArbitroId) != 0)
                ModelState.AddModelError(string.Empty, "Arbitro já associado com esta temporada, gravação não permitida");

            if (arbitroTemporada.Id != 0)
                if (db.ArbitrosTemporadas.Any(x => x.CampeonatoId == arbitroTemporada.CampeonatoId && x.TemporadaId == arbitroTemporada.TemporadaId && x.ArbitroId == arbitroTemporada.ArbitroId))
                    if (db.ArbitrosTemporadas.Where(x => x.CampeonatoId == arbitroTemporada.CampeonatoId && x.TemporadaId == arbitroTemporada.TemporadaId && x.ArbitroId == arbitroTemporada.ArbitroId).SingleOrDefault().Id != arbitroTemporada.Id)
                        ModelState.AddModelError(string.Empty, "Arbitro já associado com esta temporada, gravação não permitida");
        }
    }
}
