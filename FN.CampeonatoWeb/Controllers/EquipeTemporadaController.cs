﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Repositories;
using FN.CampeonatoWeb.Helper;

namespace FN.CampeonatoWeb.Controllers
{
    public class EquipeTemporadaController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<EquipeTemporada> equipesTemporada = new List<EquipeTemporada>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Campeonato":
                    equipesTemporada = db.EquipesTemporadas.OrderBy(s => s.Campeonato.Descricao).ToList();
                    break;
                case "Temporada":
                    equipesTemporada = db.EquipesTemporadas.OrderBy(s => s.Temporada.Descricao).ToList();
                    break;
                case "Equipe":
                    equipesTemporada = db.EquipesTemporadas.OrderBy(s => s.Equipe.Descricao).ToList();
                    break;
                default:
                    equipesTemporada = db.EquipesTemporadas.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(equipesTemporada.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        [ActionName("Edit")]
        public ActionResult IniciaEdit(EquipeTemporada equipeTemporada)
        {
            if (equipeTemporada.Id == 0)
            {
                ViewBag.Operacao = "I";
            }
            else
            {
                equipeTemporada = db.EquipesTemporadas.Find(equipeTemporada.Id);
                ViewBag.Operacao = "A";
            }

            CarregaDadosauxiliares(equipeTemporada);

            return View(equipeTemporada);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(EquipeTemporada equipeTemporada)
        {
            IsValidacao(equipeTemporada);

            if (!ModelState.IsValid)
                return View("Edit", equipeTemporada);

            if (equipeTemporada.Id == 0)
            {
                db.Entry(equipeTemporada).State = EntityState.Added;
                db.EquipesTemporadas.Add(equipeTemporada);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit", new EquipeTemporada { CampeonatoId = equipeTemporada.CampeonatoId, TemporadaId = equipeTemporada.TemporadaId });
            }
            else
            {
                db.Entry(equipeTemporada).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DropDownlistChange(EquipeTemporada equipeTemporada)
        {
            return RedirectToAction("Edit", equipeTemporada);
        }

        public ActionResult Excluir(int id)
        {
            EquipeTemporada equipeTemporada = db.EquipesTemporadas.Find(id);
            db.Entry(equipeTemporada).State = EntityState.Deleted;
            db.EquipesTemporadas.Remove(equipeTemporada);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }

        private void CarregaDadosauxiliares(EquipeTemporada equipeTemporada)
        {
            ViewBag.Campeonatos = new SelectList(db.Campeonatos.ToList(), "Id", "Descricao");

            if (equipeTemporada.CampeonatoId != 0)
            {
                var lista = (from a in db.CampeonatosTemporadas
                             join b in db.Temporadas on a.TemporadaId equals b.Id
                             where (a.CampeonatoId == equipeTemporada.CampeonatoId)
                             select new SelectListItem { Value = b.Id.ToString(), Text = b.Descricao }).ToList();

                //ViewBag.Temporadas = new SelectList(lista, "Id", "Descricao");
                ViewBag.Temporadas = lista;
            }
            else
            {
                ViewBag.Temporadas = new List<SelectListItem>();
            }

            ViewBag.Equipes = new SelectList(db.Equipes.ToList(), "Id", "Descricao");

            equipeTemporada.ListaDados = ((from a in db.EquipesTemporadas
                                            join b in db.Campeonatos on a.CampeonatoId equals b.Id
                                            join c in db.Temporadas on a.TemporadaId equals c.Id
                                            join d in db.Equipes on a.EquipeId equals d.Id
                                            where (a.CampeonatoId == equipeTemporada.CampeonatoId) &&
                                            (a.TemporadaId == equipeTemporada.TemporadaId || equipeTemporada.TemporadaId == 0)
                                            select new VisualizaLista { Campo1 = b.Descricao, Campo2 = c.Descricao, Campo3 = d.Descricao }).ToList());
        }

        public void IsValidacao(EquipeTemporada equipeTemporada)
        {
            ModelState.Clear();
            if (equipeTemporada.CampeonatoId <= 0)
                ModelState.AddModelError("CampeonatoId", "Informar o Campeonato.");

            if (equipeTemporada.TemporadaId <= 0)
                ModelState.AddModelError("TemporadaId", "Informar a Temporada.");

            if (equipeTemporada.EquipeId <= 0)
                ModelState.AddModelError("EquipeId", "Informar a Equipe.");

            if (db.CampeonatosTemporadas.Count(x => x.CampeonatoId == equipeTemporada.CampeonatoId && x.TemporadaId == equipeTemporada.TemporadaId) == 0)
                ModelState.AddModelError(string.Empty, "Temporada não associada com este campeonato, gravação não permitida");

            if (db.EquipesTemporadas.Count(x => x.CampeonatoId == equipeTemporada.CampeonatoId && x.TemporadaId == equipeTemporada.TemporadaId && x.EquipeId == equipeTemporada.EquipeId) != 0)
                ModelState.AddModelError(string.Empty, "Equipe já associado com esta temporada, gravação não permitida");

            if (equipeTemporada.Id != 0)
                if (db.EquipesTemporadas.Any(x => x.CampeonatoId == equipeTemporada.CampeonatoId && x.TemporadaId == equipeTemporada.TemporadaId && x.EquipeId == equipeTemporada.EquipeId))
                    if (db.EquipesTemporadas.Where(x => x.CampeonatoId == equipeTemporada.CampeonatoId && x.TemporadaId == equipeTemporada.TemporadaId && x.EquipeId == equipeTemporada.EquipeId).SingleOrDefault().Id != equipeTemporada.Id)
                        ModelState.AddModelError(string.Empty, "Equipe já associado com esta temporada, gravação não permitida");
        }
    }
}
