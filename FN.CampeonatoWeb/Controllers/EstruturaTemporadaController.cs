﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Repositories;
using FN.CampeonatoWeb.Helper;

namespace FN.CampeonatoWeb.Controllers
{
    public class EstruturaTemporadaController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<EstruturaTemporada> estruturaTemporada = new List<EstruturaTemporada>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Temporada":
                    estruturaTemporada = db.EstruturasTemporadas.OrderBy(s => s.Temporada.Descricao).ToList();
                    break;
                case "Fase":
                    estruturaTemporada = db.EstruturasTemporadas.OrderBy(s => s.Fase.Descricao).ToList();
                    break;
                case "Grupo":
                    estruturaTemporada = db.EstruturasTemporadas.OrderBy(s => s.Grupo.Descricao).ToList();
                    break;
                case "Rodada":
                    estruturaTemporada = db.EstruturasTemporadas.OrderBy(s => s.Rodada.Descricao).ToList();
                    break;
                default:
                    estruturaTemporada = db.EstruturasTemporadas.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(estruturaTemporada.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        [ActionName("Edit")]
        public ActionResult IniciaEdit(EstruturaTemporada estruturaTemporada)
        {
            if (estruturaTemporada.Id == 0)
            {
                ViewBag.Operacao = "I";
            }
            else
            {
                estruturaTemporada = db.EstruturasTemporadas.Find(estruturaTemporada.Id);
                ViewBag.Operacao = "A";
            }

            CarregaDadosauxiliares(estruturaTemporada);

            return View(estruturaTemporada);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(EstruturaTemporada estruturaTemporada)
        {
            IsValidacao(estruturaTemporada);

            if (!ModelState.IsValid)
                return View("Edit", estruturaTemporada);

            if (estruturaTemporada.Id == 0)
            {
                db.Entry(estruturaTemporada).State = EntityState.Added;
                db.EstruturasTemporadas.Add(estruturaTemporada);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit", new EstruturaTemporada { 
                    CampeonatoId = estruturaTemporada.CampeonatoId,
                    TemporadaId = estruturaTemporada.TemporadaId,
                    FaseId = estruturaTemporada.FaseId,
                    GrupoId = estruturaTemporada.GrupoId
                });
            }
            else
            {
                db.Entry(estruturaTemporada).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            EstruturaTemporada estruturaTemporada = db.EstruturasTemporadas.Find(id);
            db.Entry(estruturaTemporada).State = EntityState.Deleted;
            db.EstruturasTemporadas.Remove(estruturaTemporada);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DropDownlistChange(EstruturaTemporada estruturaTemporada)
        {
            return RedirectToAction("Edit", estruturaTemporada);
        }

        private void CarregaDadosauxiliares(EstruturaTemporada estruturaTemporada)
        {
            ViewBag.Campeonatos = new SelectList(db.Campeonatos.ToList(), "Id", "Descricao");

            if (estruturaTemporada.CampeonatoId != 0)
            {
                var lista = (from a in db.CampeonatosTemporadas
                             join b in db.Temporadas on a.TemporadaId equals b.Id
                             where (a.CampeonatoId == estruturaTemporada.CampeonatoId)
                             select new SelectListItem { Value = b.Id.ToString(), Text = b.Descricao }).ToList();

                //ViewBag.Temporadas = new SelectList(lista, "Id", "Descricao");
                ViewBag.Temporadas = lista;
            }
            else
            {
                ViewBag.Temporadas = new List<SelectListItem>();
            }

            ViewBag.Fases = new SelectList(db.Fases.ToList(), "Id", "Descricao");
            ViewBag.Grupos = new SelectList(db.Grupos.ToList(), "Id", "Descricao");
            ViewBag.Rodadas = new SelectList(db.Rodadas.ToList(), "Id", "Descricao");

            estruturaTemporada.ListaDados = ((from a in db.EstruturasTemporadas
                                              join b in db.Campeonatos on a.CampeonatoId equals b.Id
                                              join c in db.Temporadas on a.TemporadaId equals c.Id
                                              join d in db.Fases on a.FaseId equals d.Id
                                              join e in db.Grupos on a.GrupoId equals e.Id
                                              join f in db.Rodadas on a.RodadaId equals f.Id
                                              where (a.CampeonatoId == estruturaTemporada.CampeonatoId) &&
                                              (a.TemporadaId == estruturaTemporada.TemporadaId) &&
                                              (a.FaseId == estruturaTemporada.FaseId || estruturaTemporada.FaseId == 0) &&
                                              (a.GrupoId == estruturaTemporada.GrupoId || estruturaTemporada.GrupoId == 0)
                                              select new VisualizaLista { Campo1 = b.Descricao, Campo2 = c.Descricao, Campo3 = d.Descricao, Campo4 = e.Descricao, Campo5 = f.Descricao }).ToList());
        }
        public void IsValidacao(EstruturaTemporada estruturaTemporada)
        {
            ModelState.Clear();
            if (estruturaTemporada.CampeonatoId <= 0)
                ModelState.AddModelError("CampeonatoId", "Selecione um campeonato.");

            if (estruturaTemporada.TemporadaId <= 0)
                ModelState.AddModelError("TemporadaId", "Selecione uma Temporada.");

            if (estruturaTemporada.FaseId <= 0)
                ModelState.AddModelError("FaseId", "Selecione uma Fase.");

            if (estruturaTemporada.GrupoId <= 0)
                ModelState.AddModelError("GrupoId", "Selecione um Grupo.");

            if (estruturaTemporada.RodadaId <= 0)
                ModelState.AddModelError("RodadaId", "Selecione uma Rodada.");

            if (db.EstruturasTemporadas.Count(x => x.CampeonatoId == estruturaTemporada.CampeonatoId && 
                x.TemporadaId == estruturaTemporada.TemporadaId && 
                x.FaseId == estruturaTemporada.FaseId && 
                x.GrupoId == estruturaTemporada.GrupoId && 
                x.RodadaId == estruturaTemporada.RodadaId) != 0)
                ModelState.AddModelError(string.Empty, "Estrutura já associada, gravação não permitida");

            if (estruturaTemporada.Id != 0)
                if (db.EstruturasTemporadas.Any(x => x.CampeonatoId == estruturaTemporada.CampeonatoId &&
                    x.TemporadaId == estruturaTemporada.TemporadaId &&
                    x.FaseId == estruturaTemporada.FaseId &&
                    x.GrupoId == estruturaTemporada.GrupoId &&
                    x.RodadaId == estruturaTemporada.RodadaId))
                    if (db.EstruturasTemporadas.Where(x => x.CampeonatoId == estruturaTemporada.CampeonatoId &&
                        x.TemporadaId == estruturaTemporada.TemporadaId &&
                        x.FaseId == estruturaTemporada.FaseId &&
                        x.GrupoId == estruturaTemporada.GrupoId &&
                        x.RodadaId == estruturaTemporada.RodadaId).SingleOrDefault().Id != estruturaTemporada.Id)
                        ModelState.AddModelError(string.Empty, "Estrutura já associada, gravação não permitida");
        }
    }
}
