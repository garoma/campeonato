﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Repositories;
using FN.CampeonatoWeb.Helper;
using FN.CampeonatoWeb.ViewModel;

namespace FN.CampeonatoWeb.Controllers
{
    public class JogoCasaController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        [ActionName("Edit")]
        public ActionResult IniciaEdit(int id)
        {
            JogoViewModel jogoViewModel = new JogoViewModel();
            JogoEquipe jogo = new JogoEquipe();
            jogo = db.JogosEquipes.SingleOrDefault(x => x.JogoId == id && x.Tipo == 0);
            jogoViewModel.JogoId = id;

            if (jogo == null)
            {
                ViewBag.Operacao = "I";
                jogoViewModel.JogoEquipeCasaId = 0;
            }
            else
            {
                jogoViewModel.JogoEquipeCasaId = jogo.Id;
                jogoViewModel.EquipeCasaId = jogo.EquipeId;
                jogoViewModel.GolsCasa = jogo.Gols;
                ViewBag.Operacao = "A";
            }

            CarregaDadosAuxiliares(jogoViewModel);
            return View("Edit", jogoViewModel); 
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DropDownlistChange(JogoViewModel jogoViewModel)
        {
            CarregaDadosAuxiliares(jogoViewModel);
            return View("Edit", jogoViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Save(JogoViewModel jogoViewModel)
        {
            JogoEquipe jogo;
            if (jogoViewModel.JogoEquipeCasaId == 0)
            {
                jogo = new JogoEquipe();
                jogo.JogoId = jogoViewModel.JogoId;
                jogo.Gols = jogoViewModel.GolsCasa;
                db.Entry(jogo).State = EntityState.Added;
                db.JogosEquipes.Add(jogo);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
            }
            else
            {
                jogo = db.JogosEquipes.SingleOrDefault(x => x.Id == jogoViewModel.JogoEquipeCasaId);
                jogo.JogoId = jogoViewModel.JogoId;
                jogo.Gols = jogoViewModel.GolsCasa;
                db.Entry(jogo).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
            }
            return RedirectToAction("Edit", "JogoCasaEvento", new { id = jogoViewModel.JogoId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Excluir")]
        public ActionResult Excluir(JogoViewModel jogoViewModel)
        {
            return RedirectToAction("List");
        }

        private void CarregaDadosAuxiliares(JogoViewModel jogoViewModel)
        {
            ViewBag.Equipes = new SelectList(db.Equipes.ToList(), "Id", "Descricao");
            
            if (jogoViewModel.EquipeForaId > 0)
                jogoViewModel.ClubeForaId = db.Equipes.SingleOrDefault(x => x.Id == jogoViewModel.EquipeForaId).ClubeId;
        }

        public void IsValidacao(JogoViewModel jogoViewModel)
        {
            ModelState.Remove("EquipeCasaId");
            ModelState.Remove("GolsCasa");

            if (jogoViewModel.EquipeCasaId == 0)
                ModelState.AddModelError("EquipeCasaId", "Campo de preenchimento obrigatorio.");

            if (jogoViewModel.GolsCasa == 0)
                ModelState.AddModelError("GolsCasa", "Campo de preenchimento obrigatorio.");
        }

        public ActionResult RetrieveClubeImage(int id)
        {
            try
            {
                var q = from temp in db.Clubes where temp.Id == id select temp.Foto;
                byte[] cover = q.First();
                return File(cover, "image/jpg");
            }
            catch
            {
                return null;
            }
        }
    }
}
