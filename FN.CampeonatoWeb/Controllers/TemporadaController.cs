﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Helper;

namespace FN.CampeonatoWeb.Controllers
{
    public class TemporadaController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<Temporada> temporada = new List<Temporada>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Descricao":
                    temporada = db.Temporadas.OrderBy(s => s.Descricao).ToList();
                    break;
                case "FormulaDisputa":
                    temporada = db.Temporadas.OrderBy(s => s.FormulaDisputa).ToList();
                    break;
                default:
                    temporada = db.Temporadas.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(temporada.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        public ActionResult Edit(int? id)
        {
            Temporada temporada;

            if (id == null || id == 0)
                temporada = new Temporada();
            else
                temporada = db.Temporadas.Find(id); 

            ViewBag.CriterioDesempate = temporada.OpcoesCriterioDesempate;
            return View(temporada);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(Temporada temporada)
        {
            //ModelState.Remove("CriterioDesempate");

            IsValidacao(temporada);

            if (!ModelState.IsValid)
                return View("Edit", temporada);

            if (temporada.Id == 0)
            {
                db.Entry(temporada).State = EntityState.Added;
                db.Temporadas.Add(temporada);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit");
            }
            else
            {
                db.Entry(temporada).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            Temporada temporada = db.Temporadas.Find(id);
            db.Entry(temporada).State = EntityState.Deleted;
            db.Temporadas.Remove(temporada);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }
        
        public void IsValidacao(Temporada temporada)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(temporada.Descricao))
                ModelState.AddModelError("Descricao", "Campo de preenchimento obrigatório.");

            if (temporada.PontosVitoria < 0)
                ModelState.AddModelError("PontosVitoria", "Informar um número maior que zero.");

            if (temporada.PontosEmpate < 0)
                ModelState.AddModelError("PontosEmpate", "Informar um número maior que zero.");

            if (temporada.PontosVitoriaProrrogacao < 0)
                ModelState.AddModelError("PontosVitoriaProrrogacao", "Informar um número maior que zero.");

            if (temporada.PontosEmpateProgrrogacao < 0)
                ModelState.AddModelError("PontosEmpateProgrrogacao", "Informar um número maior que zero.");

            if (temporada.PontosDerrotaProrrogacao < 0)
                ModelState.AddModelError("PontosDerrotaProrrogacao", "Informar um número maior que zero.");

            if (temporada.PontosVitoriaPenaltis < 0)
                ModelState.AddModelError("PontosVitoriaPenaltis", "Informar um número maior que zero.");

            if (temporada.PontosDerrotaPenaltis < 0)
                ModelState.AddModelError("PontosDerrotaPenaltis", "Informar um número maior que zero.");

            if (string.IsNullOrEmpty(temporada.CriterioDesempate))
                ModelState.AddModelError("CriterioDesempate", "Campo de preenchimento obrigatório.");

            if (temporada.NumTimePromovido < 0)
                ModelState.AddModelError("NumTimePromovido", "Informar um número maior que zero.");

            if (temporada.NumTimeCompete1 < 0)
                ModelState.AddModelError("NumTimeCompete1", "Informar um número maior que zero.");

            if (temporada.NumTimeCompete2 < 0)
                ModelState.AddModelError("NumTimeCompete2", "Informar um número maior que zero.");

            if (temporada.NumTimeRebaixamento < 0)
                ModelState.AddModelError("NumTimeRebaixamento", "Informar um número maior que zero.");

            if (temporada.NumTimeRebaixado < 0)
                ModelState.AddModelError("NumTimeRebaixado", "Informar um número maior que zero.");
        }
    }
}
