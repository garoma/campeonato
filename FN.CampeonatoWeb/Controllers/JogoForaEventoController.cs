﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Repositories;
using FN.CampeonatoWeb.Helper;
using FN.CampeonatoWeb.ViewModel;

namespace FN.CampeonatoWeb.Controllers
{
    public class JogoForaEventoController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        [ActionName("Edit")]
        public ActionResult IniciaEdit(int id, int? idEvento)
        {
            JogoViewModel jogoViewModel = CarregaEventoFora(id, idEvento);
            CarregaDadosAuxiliares(jogoViewModel);
            return View("Edit", jogoViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DropDownlistChange(JogoViewModel jogoViewModel)
        {
            jogoViewModel = CarregaEventoFora(jogoViewModel.JogoId, null);
            CarregaDadosAuxiliares(jogoViewModel);
            return View("Edit", jogoViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "IncluirEvento")]
        public ActionResult IncluirEvento(JogoViewModel jogoViewModel)
        {
            IsValidacao(jogoViewModel);
            jogoViewModel.AlteraEventoForaId = 0;

            JogoEvento eventoFora = new JogoEvento
            {
                JogoId = jogoViewModel.JogoId,
                JogadorId = jogoViewModel.JogadorForaId,
                EventoId = jogoViewModel.EventoForaId,
                TempoId = jogoViewModel.TempoForaId,
                Minuto = jogoViewModel.MinutoFora,
                Quantidade = jogoViewModel.QuantidadeFora
            };

            if (jogoViewModel.AlteraEventoForaId == 0)
            {
                db.Entry(eventoFora).State = EntityState.Added;
                db.JogosEventos.Add(eventoFora);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
            }
            else
            {
                db.Entry(eventoFora).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
            }
            CarregaDadosAuxiliares(jogoViewModel);
            return RedirectToAction("Edit", "JogoForaEvento", new { id = jogoViewModel.JogoId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Excluir")]
        public ActionResult Excluir(int idJogoEvento)
        {
            JogoEvento jogo = new JogoEvento();

            jogo = db.JogosEventos.Find(idJogoEvento);
            db.Entry(jogo).State = EntityState.Deleted;
            db.JogosEventos.Remove(jogo);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("Edit", "JogoForaEvento", new { id = jogo.JogoId });
        }

        private void CarregaDadosAuxiliares(JogoViewModel jogoViewModel)
        {
            ViewBag.Jogadores = new SelectList(db.Jogadores.ToList(), "Id", "Nome");
            ViewBag.Eventos = new SelectList(db.Eventos.ToList(), "Id", "Descricao");
            ViewBag.Tempos = new SelectList(db.Tempos.ToList(), "Id", "Descricao");
        }

        private JogoViewModel CarregaEventoFora(int id, int? idEvento)
        {
            JogoViewModel jogoViewModel = new JogoViewModel();
            List<JogoEvento> jogo = new List<JogoEvento>();

            jogo = db.JogosEventos.Where(x => x.JogoId == id).ToList();
            jogoViewModel.JogoId = id;
            jogoViewModel.AlteraEventoForaId = idEvento != null ? idEvento.Value : 0;

            if (jogo.Count <= 0)
                return jogoViewModel;

            jogoViewModel.EventosFora = new List<JogoEvento>();

            foreach (JogoEvento item in jogo)
            {
                if (jogoViewModel.AlteraEventoForaId == item.EventoId)
                {
                    jogoViewModel.JogadorForaId = item.JogadorId;
                    jogoViewModel.EventoForaId = item.EventoId;
                    jogoViewModel.TempoForaId = item.TempoId;
                    jogoViewModel.MinutoFora = item.Minuto;
                    jogoViewModel.QuantidadeFora = item.Quantidade;
                }

                jogoViewModel.EventosFora.Add(
                    new JogoEvento
                    {
                        Id = item.Id,
                        JogoId = id,
                        JogadorId = item.JogadorId,
                        EventoId = item.EventoId,
                        TempoId = item.TempoId,
                        Minuto = item.TempoId,
                        Quantidade = item.Quantidade,
                        Jogador = new Jogador { Nome = db.Jogadores.SingleOrDefault(x => x.Id == item.JogadorId).Nome },
                        Evento = new Evento { Descricao = db.Eventos.SingleOrDefault(x => x.Id == item.EventoId).Descricao },
                        Tempo = new Tempo { Descricao = db.Tempos.SingleOrDefault(x => x.Id == item.TempoId).Descricao }
                    });
            }
            return jogoViewModel;
        }
        public void IsValidacao(JogoViewModel jogoViewModel)
        {
            if (jogoViewModel.JogadorForaId == 0)
                ModelState.AddModelError("JogadorForaId", "Campo de preenchimento obrigatorio.");

            if (jogoViewModel.EventoForaId == 0)
                ModelState.AddModelError("EventoForaId", "Campo de preenchimento obrigatorio.");

            if (jogoViewModel.TempoForaId == 0)
                ModelState.AddModelError("TempoForaId", "Campo de preenchimento obrigatorio.");

            if (jogoViewModel.MinutoFora <= 0)
                ModelState.AddModelError("MinutoFora", "Campo de preenchimento obrigatorio.");

            if (jogoViewModel.QuantidadeFora <= 0)
                ModelState.AddModelError("QuantidadeFora", "Campo de preenchimento obrigatorio.");
        }
        public ActionResult RetrieveJogadorImage(int id)
        {
            try
            {
                var q = from temp in db.Jogadores where temp.Id == id select temp.Foto;
                byte[] cover = q.First();
                return File(cover, "image/jpg");
            }
            catch
            {
                return null;
            }
        }
    }
}
