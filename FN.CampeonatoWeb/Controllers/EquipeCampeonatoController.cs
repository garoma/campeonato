﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Repositories;
using FN.CampeonatoWeb.Helper;

namespace FN.CampeonatoWeb.Controllers
{
    public class EquipeCampeonatoController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<EquipeCampeonato> equipeCampeonato = new List<EquipeCampeonato>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Campeonato":
                    equipeCampeonato = db.EquipesCampeonatos.OrderBy(s => s.Campeonato.Descricao).ToList();
                    break;
                case "Equipe":
                    equipeCampeonato = db.EquipesCampeonatos.OrderBy(s => s.Equipe.Descricao).ToList();
                    break;
                default:
                    equipeCampeonato = db.EquipesCampeonatos.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(equipeCampeonato.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        [ActionName("Edit")]
        public ActionResult IniciaEdit(EquipeCampeonato equipeCampeonato)
        {
            if (equipeCampeonato.Id == 0)
            {
                ViewBag.Operacao = "I";
            }
            else
            {
                equipeCampeonato = db.EquipesCampeonatos.Find(equipeCampeonato.Id);
                ViewBag.Operacao = "A";
            }

            CarregaDadosauxiliares(equipeCampeonato);
            return View(equipeCampeonato);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(EquipeCampeonato equipeCampeonato)
        {
            IsValidacao(equipeCampeonato);

            if (!ModelState.IsValid)
                return View("Edit", equipeCampeonato);

            if (equipeCampeonato.Id == 0)
            {
                db.Entry(equipeCampeonato).State = EntityState.Added;
                db.EquipesCampeonatos.Add(equipeCampeonato);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit", new EquipeTemporada { CampeonatoId = equipeCampeonato.CampeonatoId });
            }
            else
            {
                db.Entry(equipeCampeonato).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            EquipeCampeonato equipeCampeonato = db.EquipesCampeonatos.Find(id);
            db.Entry(equipeCampeonato).State = EntityState.Deleted;
            db.EquipesCampeonatos.Remove(equipeCampeonato);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DropDownlistChange(EquipeCampeonato equipeCampeonato)
        {
            return RedirectToAction("Edit", equipeCampeonato);
        }

        private void CarregaDadosauxiliares(EquipeCampeonato equipeCampeonato)
        {
            ViewBag.Campeonatos = db.Campeonatos.Select(d => new SelectListItem { Value = d.Id.ToString(), Text = d.Descricao }).ToList();
            ViewBag.Equipes = new SelectList(db.Equipes.ToList(), "Id", "Descricao");

            equipeCampeonato.ListaDados = (from a in db.EquipesCampeonatos
                                            join b in db.Campeonatos on a.CampeonatoId equals b.Id
                                            join c in db.Equipes on a.EquipeId equals c.Id
                                            where (a.CampeonatoId == equipeCampeonato.CampeonatoId)
                                            select new VisualizaLista { Campo1 = b.Descricao, Campo2 = c.Descricao }).ToList();
        }

        public void IsValidacao(EquipeCampeonato equipeCampeonato)
        {
            if (equipeCampeonato.CampeonatoId <= 0)
                ModelState.AddModelError("CampeonatoId", "Selecione o Campeonato.");

            if (equipeCampeonato.EquipeId <= 0)
                ModelState.AddModelError("EquipeId", "Selecone uma Equipe.");

            if (db.EquipesCampeonatos.Count(x => x.CampeonatoId == equipeCampeonato.CampeonatoId && x.EquipeId == equipeCampeonato.EquipeId) != 0 && equipeCampeonato.Id == 0)
                ModelState.AddModelError(string.Empty, "Temporada já associada com o campeonato, gravação não permitida");

            if (equipeCampeonato.Id != 0)
                if (db.EquipesCampeonatos.Any(x => x.CampeonatoId == equipeCampeonato.CampeonatoId && x.EquipeId == equipeCampeonato.EquipeId))
                    if (db.EquipesCampeonatos.Where(x => x.CampeonatoId == equipeCampeonato.CampeonatoId && x.EquipeId == equipeCampeonato.EquipeId).SingleOrDefault().Id != equipeCampeonato.Id)
                        ModelState.AddModelError(string.Empty, "Temporada já associada com o campeonato, gravação não permitida");
        }
    }
}
