﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Repositories;
using FN.CampeonatoWeb.Helper;

namespace FN.CampeonatoWeb.Controllers
{
    public class EstadioController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;

            List<Estadio> estadio = new List<Estadio>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Nome":
                    estadio = db.Estadios.OrderBy(s => s.Nome).ToList();
                    break;
                case "Descricao":
                    estadio = db.Estadios.OrderBy(s => s.Descricao).ToList();
                    break;
                case "Capacidade":
                    estadio = db.Estadios.OrderBy(s => s.Capacidade).ToList();
                    break;
                case "Pais":
                    estadio = db.Estadios.OrderBy(s => s.Pais).ToList();
                    break;
                default:
                    estadio = db.Estadios.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(estadio.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        public ActionResult Edit(int? id)
        {
            Estadio estadio;

            if (id == null || id == 0)
                estadio = new Estadio();
            else
                estadio = db.Estadios.Find(id);

            return View(estadio);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(Estadio estadio)
        {
            IsValidacao(estadio);

            if (!ModelState.IsValid)
                return View("Edit", estadio);

            HttpPostedFileBase file = Request.Files["ImageData"];
            ContentRepository service = new ContentRepository();
            estadio.Foto = service.UploadImageInDataBase(file);

            if (estadio.Id == 0)
            {
                db.Entry(estadio).State = EntityState.Added;
                db.Estadios.Add(estadio);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit");
            }
            else
            {
                db.Entry(estadio).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            Estadio estadio = db.Estadios.Find(id);
            db.Entry(estadio).State = EntityState.Deleted;
            db.Estadios.Remove(estadio);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }

        public void IsValidacao(Estadio estadio)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(estadio.Descricao))
                ModelState.AddModelError("Descricao", "Campo de preenchimento obrigatório.");

            if (string.IsNullOrEmpty(estadio.Nome))
                ModelState.AddModelError("Nome", "Campo de preenchimento obrigatório.");

            if (estadio.Capacidade < 0)
                ModelState.AddModelError("Capacidade", "Informar um número maior que zero.");

            if (string.IsNullOrEmpty(estadio.Pais))
                ModelState.AddModelError("Pais", "Campo de preenchimento obrigatório.");
        }

        public ActionResult RetrieveImage(int id)
        {
            byte[] cover = GetImageFromDataBase(id);
            if (cover != null)
            {
                return File(cover, "image/jpg");
            }
            else
            {
                return null;
            }
        }

        public byte[] GetImageFromDataBase(int Id)
        {
            try
            {
                var q = from temp in db.Estadios where temp.Id == Id select temp.Foto;
                byte[] cover = q.First();
                return cover;
            }
            catch
            {
                return null;
            }
        }
    }
}
