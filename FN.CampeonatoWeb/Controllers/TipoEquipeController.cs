﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;

namespace FN.CampeonatoWeb.Controllers
{
    public class TipoEquipeController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<TipoEquipe> tipoEquipe = new List<TipoEquipe>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Descricao":
                    tipoEquipe = db.TiposEquipes.OrderBy(s => s.Descricao).ToList();
                    break;
                default:
                    tipoEquipe = db.TiposEquipes.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(tipoEquipe.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        public ActionResult Edit(int? id)
        {
            TipoEquipe tiposEquipe;

            if (id == null || id == 0)
                tiposEquipe = new TipoEquipe();
            else
                tiposEquipe = db.TiposEquipes.Find(id);

            return View(tiposEquipe);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(TipoEquipe tipoEquipe)
        {
            IsValidacao(tipoEquipe);

            if (!ModelState.IsValid)
                return View("Edit", tipoEquipe);

            if (tipoEquipe.Id == 0)
            {
                db.Entry(tipoEquipe).State = EntityState.Added;
                db.TiposEquipes.Add(tipoEquipe);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit");
            }
            else
            {
                db.Entry(tipoEquipe).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        [ValidateAntiForgeryToken]
        public ActionResult Excluir(TipoEquipe tipoEquipe)
        {
            db.Entry(tipoEquipe).State = EntityState.Deleted;
            db.TiposEquipes.Remove(tipoEquipe);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }
        public void IsValidacao(TipoEquipe tipoEquipe)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(tipoEquipe.Descricao))
                ModelState.AddModelError("Descricao", "Campo de preenchimento obrigatório.");
        }

    }
}
