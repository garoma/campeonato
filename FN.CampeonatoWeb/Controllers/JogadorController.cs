﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Repositories;
using FN.CampeonatoWeb.Helper;

namespace FN.CampeonatoWeb.Controllers
{
    public class JogadorController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            ViewBag.Posicoes = db.PosicoesJogadores.Select(d => new SelectListItem { Value = d.Id.ToString(), Text = d.Descricao }).ToList();

            List<Jogador> jogadores = new List<Jogador>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Apelido":
                    jogadores = db.Jogadores.OrderBy(s => s.Apelido).ToList();
                    break;
                case "Nome":
                    jogadores = db.Jogadores.OrderBy(s => s.Nome).ToList();
                    break;
                case "NumeroCamisa":
                    jogadores = db.Jogadores.OrderBy(s => s.NumeroCamisa).ToList();
                    break;
                case "PosicaoJogador":
                    jogadores = db.Jogadores.OrderBy(s => s.PosicaoJogador).ToList();
                    break;
                case "Nascimento":
                    jogadores = db.Jogadores.OrderBy(s => s.Nascimento).ToList();
                    break;
                default:
                    jogadores = db.Jogadores.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(jogadores.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        public ActionResult Edit(int? id)
        {
            Jogador jogador;

            if (id == null || id == 0)
                jogador = new Jogador();
            else
                jogador = db.Jogadores.Find(id);

            ViewBag.Posicoes = db.PosicoesJogadores.Select(d => new SelectListItem { Value = d.Id.ToString(), Text = d.Descricao }).ToList();

            return View(jogador);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(Jogador jogador)
        {
            IsValidacao(jogador);

            if (!ModelState.IsValid)
                return View("Edit", jogador);

            HttpPostedFileBase file = Request.Files["ImageData"];
            ContentRepository service = new ContentRepository();
            jogador.Foto = service.UploadImageInDataBase(file);

            if (jogador.Id == 0)
            {
                db.Jogadores.Add(jogador);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit");
            }
            else
            {
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            Jogador jogador = db.Jogadores.Find(id);
            db.Entry(jogador).State = EntityState.Deleted;
            db.Jogadores.Remove(jogador);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }

        public void IsValidacao(Jogador jogador)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(jogador.Apelido))
                ModelState.AddModelError("Apelido", "Campo de preenchimento obrigatório.");

            if (string.IsNullOrEmpty(jogador.Nome))
                ModelState.AddModelError("Nome", "Campo de preenchimento obrigatório.");

            if (jogador.NumeroCamisa < 0)
                ModelState.AddModelError("NumeroCamisa", "Informar um número maior que zero.");

            if (jogador.PosicaoJogadorId < 0)
                ModelState.AddModelError("PosicaoJogadorId", "Selecionar uma posição para o jogador.");

            if (jogador.Nascimento == DateTime.MinValue)
                ModelState.AddModelError("Nascimento", "Informar a Data de Nascimento do jogador.");

            if (string.IsNullOrEmpty(jogador.EMail))
                ModelState.AddModelError("EMail", "Campo de preenchimento obrigatório.");

            if (string.IsNullOrEmpty(jogador.Nacionalidade))
                ModelState.AddModelError("Nacionalidade", "Campo de preenchimento obrigatório.");
        }

        public ActionResult RetrieveImage(int id)
        {
            byte[] cover = GetImageFromDataBase(id);
            if (cover != null)
            {
                return File(cover, "image/jpg");
            }
            else
            {
                return null;
            }
        }

        public byte[] GetImageFromDataBase(int Id)
        {
            try
            {
                var q = from temp in db.Jogadores where temp.Id == Id select temp.Foto;
                byte[] cover = q.First();
                return cover;
            }
            catch
            {
                return null;
            }
        }
    }
}
