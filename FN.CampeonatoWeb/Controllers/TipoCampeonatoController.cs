﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;

namespace FN.CampeonatoWeb.Controllers
{
    public class TipoCampeonatoController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<TipoCampeonato> tipoCampeonato = new List<TipoCampeonato>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Descricao":
                    tipoCampeonato = db.TiposCampeonatos.OrderBy(s => s.Descricao).ToList();
                    break;
                default:
                    tipoCampeonato = db.TiposCampeonatos.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(tipoCampeonato.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        public ActionResult Edit(int? id)
        {
            TipoCampeonato tipoCampeonato;

            if (id == null || id == 0)
                tipoCampeonato = new TipoCampeonato();
            else
                tipoCampeonato = db.TiposCampeonatos.Find(id);

            return View(tipoCampeonato);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(TipoCampeonato tipoCampeonato)
        {
            IsValidacao(tipoCampeonato);

            if (!ModelState.IsValid)
                return View("Edit", tipoCampeonato);

            if (tipoCampeonato.Id == 0)
            {
                db.Entry(tipoCampeonato).State = EntityState.Added;
                db.TiposCampeonatos.Add(tipoCampeonato);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit");
            }
            else
            {
                db.Entry(tipoCampeonato).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            TipoCampeonato tipoCampeonato = db.TiposCampeonatos.Find(id);
            db.Entry(tipoCampeonato).State = EntityState.Deleted;
            db.TiposCampeonatos.Remove(tipoCampeonato);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }
        public void IsValidacao(TipoCampeonato tipoCampeonato)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(tipoCampeonato.Descricao))
                ModelState.AddModelError("Descricao", "Campo de preenchimento obrigatório.");
        }

    }
}
