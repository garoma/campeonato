﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;

namespace FN.CampeonatoWeb.Controllers
{
    public class TempoController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<Tempo> tempo = new List<Tempo>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Descricao":
                    tempo = db.Tempos.OrderBy(s => s.Descricao).ToList();
                    break;
                default:
                    tempo = db.Tempos.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(tempo.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        public ActionResult Edit(int? id)
        {
            Tempo tempo;

            if (id == null || id == 0)
                tempo = new Tempo();
            else
                tempo = db.Tempos.Find(id);

            return View(tempo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(Tempo tempo)
        {
            IsValidacao(tempo);

            if (!ModelState.IsValid)
                return View("Edit", tempo);

            if (tempo.Id == 0)
            {
                db.Entry(tempo).State = EntityState.Added;
                db.Tempos.Add(tempo);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit");
            }
            else
            {
                db.Entry(tempo).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            Tempo tempo = db.Tempos.Find(id);
            db.Entry(tempo).State = EntityState.Deleted;
            db.Tempos.Remove(tempo);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }
        
        public void IsValidacao(Tempo tempo)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(tempo.Descricao))
                ModelState.AddModelError("Descricao", "Campo de preenchimento obrigatório.");
        }
    }
}
