﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Repositories;
using FN.CampeonatoWeb.Helper;

namespace FN.CampeonatoWeb.Controllers
{
    public class ClubeController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            ViewBag.Posicoes = db.Estadios.Select(d => new SelectListItem { Value = d.Id.ToString(), Text = d.Nome }).ToList();

            List<Clube> clube = new List<Clube>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Nome":
                    clube = db.Clubes.OrderBy(s => s.Nome).ToList();
                    break;
                case "Descricao":
                    clube = db.Clubes.OrderBy(s => s.Descricao).ToList();
                    break;
                case "DataFundacao":
                    clube = db.Clubes.OrderBy(s => s.DataFundacao).ToList();
                    break;
                case "Estadio":
                    clube = db.Clubes.OrderBy(s => s.Estadio).ToList();
                    break;
                case "Cidade":
                    clube = db.Clubes.OrderBy(s => s.Cidade).ToList();
                    break;
                case "Pais":
                    clube = db.Clubes.OrderBy(s => s.Pais).ToList();
                    break;
                default:
                    clube = db.Clubes.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(clube.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        public ActionResult Edit(int? id)
        {
            Clube clube;

            if (id == null || id == 0)
                clube = new Clube();
            else
                clube = db.Clubes.Find(id);

            ViewBag.Estadios = db.Estadios.Select(d => new SelectListItem { Value = d.Id.ToString(), Text = d.Descricao }).ToList();
            return View(clube);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(Clube clube)
        {
            IsValidacao(clube);

            if (!ModelState.IsValid)
            {
                ViewBag.Estadios = new SelectList(db.Estadios.ToList(), "Id", "Descricao");
                return View("Edit", clube);
            }

            HttpPostedFileBase file = Request.Files["ImageData"];
            ContentRepository service = new ContentRepository();
            clube.Foto = service.UploadImageInDataBase(file);

            if (clube.Id == 0)
            {
                db.Entry(clube).State = EntityState.Added;
                db.Clubes.Add(clube);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit");
            }
            else
            {
                db.Entry(clube).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            Clube clube = db.Clubes.Find(id);
            db.Entry(clube).State = EntityState.Deleted;
            db.Clubes.Remove(clube);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }
        
        public void IsValidacao(Clube clube)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(clube.Nome))
                ModelState.AddModelError("Nome", "Campo de preenchimento obrigatório.");

            if (string.IsNullOrEmpty(clube.Descricao))
                ModelState.AddModelError("Descricao", "Campo de preenchimento obrigatório.");

            if (clube.DataFundacao == DateTime.MinValue)
                ModelState.AddModelError("DataFundacao", "Informe a Data de Fundação.");

            if (string.IsNullOrEmpty(clube.Cidade))
                ModelState.AddModelError("Cidade", "Campo de preenchimento obrigatório.");
            
            if (string.IsNullOrEmpty(clube.Pais))
                ModelState.AddModelError("Pais", "Campo de preenchimento obrigatório.");

            if (clube.EstadioId <= 0)
                ModelState.AddModelError("EstadioId", "Selecione um Estadio.");
        }
        
        public ActionResult RetrieveClubeImage(int id)
        {
            try
            {
                var q = from temp in db.Clubes where temp.Id == id select temp.Foto;
                byte[] cover = q.First();
                return File(cover, "image/jpg");
            }
            catch
            {
                return null;
            }
        }
    }
}
