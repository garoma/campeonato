﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;
using FN.CampeonatoWeb.Repositories;
using FN.CampeonatoWeb.Helper;

namespace FN.CampeonatoWeb.Controllers
{
    public class ArbitroController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            var _tipos = from TipoArbitro d in System.Enum.GetValues(typeof(TipoArbitro))
                         select new { ID = (int)d, Name = d.ToString() };
            ViewData["TipoArbitro"] = new SelectList(_tipos, "ID", "Name");

            List<Arbitro> arbitro = new List<Arbitro>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Nome":
                    arbitro = db.Arbitros.OrderBy(s => s.Nome).ToList();
                    break;
                case "Nascimento":
                    arbitro = db.Arbitros.OrderBy(s => s.Nascimento).ToList();
                    break;
                case "Tipo":
                    arbitro = db.Arbitros.OrderBy(s => s.Tipo).ToList();
                    break;
                default:
                    arbitro = db.Arbitros.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(arbitro.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        public ActionResult Edit(int? id)
        {
            Arbitro arbitro;

            if (id == null || id == 0)
                arbitro = new Arbitro();
            else
                arbitro = db.Arbitros.Find(id);

            var _tipos = from TipoArbitro d in System.Enum.GetValues(typeof(TipoArbitro))
                         select new { ID = (int)d, Name = d.ToString() };

            ViewData["TipoArbitro"] = new SelectList(_tipos, "ID", "Name");

            return View(arbitro);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(Arbitro arbitro)
        {
            IsValidacao(arbitro);

            if (!ModelState.IsValid)
                return View("Edit", arbitro);

            HttpPostedFileBase file = Request.Files["ImageData"];
            ContentRepository service = new ContentRepository();
            arbitro.Foto = service.UploadImageInDataBase(file);

            if (arbitro.Id == 0)
            {
                db.Entry(arbitro).State = EntityState.Added;
                db.Arbitros.Add(arbitro);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit", arbitro);
            }
            else
            {
                db.Entry(arbitro).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            Arbitro arbitro = db.Arbitros.Find(id);
            db.Entry(arbitro).State = EntityState.Deleted;
            db.Arbitros.Remove(arbitro);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }

        public void IsValidacao(Arbitro arbitro)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(arbitro.Nome))
                ModelState.AddModelError("Nome", "Campo de preenchimento obrigatório.");
            
            if (arbitro.Nascimento == DateTime.MinValue)
                ModelState.AddModelError("Nome", "Informar a Data de Nascimeno do Arbitro.");
            
            if (arbitro.Tipo <= 0)
                ModelState.AddModelError("Tipo", "Selecione um Tipo de Arbitro.");
        }

        public ActionResult RetrieveImage(int id)
        {
            byte[] cover = GetImageFromDataBase(id);
            if (cover != null)
            {
                return File(cover, "image/jpg");
            }
            else
            {
                return null;
            }
        }

        public byte[] GetImageFromDataBase(int Id)
        {
            try
            {
                var q = from temp in db.Arbitros where temp.Id == Id select temp.Foto;
                byte[] cover = q.First();
                return cover;
            }
            catch
            {
                return null;
            }
        }
    }
}
