﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;

namespace FN.CampeonatoWeb.Controllers
{
    public class FaseController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<Fase> fase = new List<Fase>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Descricao":
                    fase = db.Fases.OrderBy(s => s.Descricao).ToList();
                    break;
                default:
                    fase = db.Fases.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(fase.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        public ActionResult Edit(int? id)
        {
            Fase fase;

            if (id == null || id == 0)
                fase = new Fase();
            else
                fase = db.Fases.Find(id);

            return View(fase);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(Fase fase)
        {
            IsValidacao(fase);

            if (!ModelState.IsValid)
                return View("Edit", fase);

            if (fase.Id == 0)
            {
                db.Entry(fase).State = EntityState.Added;
                db.Fases.Add(fase);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit");
            }
            else
            {
                db.Entry(fase).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            Fase fase = db.Fases.Find(id);
            db.Entry(fase).State = EntityState.Deleted;
            db.Fases.Remove(fase);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }
        public void IsValidacao(Fase fase)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(fase.Descricao))
                ModelState.AddModelError("Descricao", "Campo de preenchimento obrigatório.");
        }
    }
}
