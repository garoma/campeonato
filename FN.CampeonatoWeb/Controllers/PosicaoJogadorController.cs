﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FN.CampeonatoWeb.Models;
using PagedList;

namespace FN.CampeonatoWeb.Controllers
{
    public class PosicaoJogadorController : BaseController
    {
        private FNCampeonatoCtx db = new FNCampeonatoCtx();

        public ActionResult List(string sortOrder, int? Page_No)
        {
            ViewBag.CurrentSortOrder = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
            List<PosicaoJogador> posicaoJogador = new List<PosicaoJogador>();

            switch (String.IsNullOrEmpty(sortOrder) ? "" : sortOrder)
            {
                case "Descricao":
                    posicaoJogador = db.PosicoesJogadores.OrderBy(s => s.Descricao).ToList();
                    break;
                default:
                    posicaoJogador = db.PosicoesJogadores.OrderBy(s => s.Id).ToList();
                    break;
            }
            int Size_Of_Page = 20;
            int No_Of_Page = (Page_No ?? 1);
            return View(posicaoJogador.ToPagedList(No_Of_Page, Size_Of_Page));
        }

        public ActionResult Edit(int? id)
        {
            PosicaoJogador posicaoJogador;

            if (id == null || id == 0)
                posicaoJogador = new PosicaoJogador();
            else
                posicaoJogador = db.PosicoesJogadores.Find(id);

            return View(posicaoJogador);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(PosicaoJogador posicaoJogador)
        {
            IsValidacao(posicaoJogador);

            if (!ModelState.IsValid)
                return View("Edit", posicaoJogador);

            if (posicaoJogador.Id == 0)
            {
                db.Entry(posicaoJogador).State = EntityState.Added;
                db.PosicoesJogadores.Add(posicaoJogador);
                db.SaveChanges();
                Success("Registro incluido com sucesso.", true);
                return RedirectToAction("Edit");
            }
            else
            {
                db.Entry(posicaoJogador).State = EntityState.Modified;
                db.SaveChanges();
                Success("Registro alterado com sucesso.", true);
                return RedirectToAction("List");
            }
        }

        public ActionResult Excluir(int id)
        {
            PosicaoJogador posicaoJogador = db.PosicoesJogadores.Find(id);
            db.Entry(posicaoJogador).State = EntityState.Deleted;
            db.PosicoesJogadores.Remove(posicaoJogador);
            db.SaveChanges();
            Success("Registro excluido com sucesso.", true);
            return RedirectToAction("List");
        }
        
        public void IsValidacao(PosicaoJogador posicaoJogador)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(posicaoJogador.Descricao))
                ModelState.AddModelError("Descricao", "Campo de preenchimento obrigatório.");
        }
    }
}
