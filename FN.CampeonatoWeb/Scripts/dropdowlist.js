/*!
 * Manipulacao de objeto DropDownList
 */

if (typeof jQuery === 'undefined') {
  throw new Error('Bootstrap\'s JavaScript requires jQuery')
}

function HabilitaDropDownList(dropdownlist) {
    $("#" + dropdownlist).prop("disabled", false);
}

function DesabilitaCampo(campo) {
    $("#" + campo).prop("disabled", true);
}

function LimpaDropDownList(form, dropdownlist) {
    select = $("#" + form + " select#" + dropdownlist);
    select.empty();
    select.append(new Option("Selecione", "0", true, true));
}

function SelecionaDropDownList(form, dropdownlist, codigo) {
    var desmarcar = ItemSelecionadoDropDownList(dropdownlist) - 1;
    $("#" + dropdownlist).find("" + "option:eq(" + desmarcar + ")" + "").attr("selected", false);
    $("#" + dropdownlist).find("" + "option:eq(" + (codigo - 1) + ")" + "").attr("selected", true);
}

function ItemSelecionadoDropDownList(dropdownlist) {
    var codigo = 0;
    $("#" + dropdownlist + " option:selected").each(function () {
        codigo = $("#" + dropdownlist).val();
    });
    if (codigo == "")
        return 0;

    return codigo;
}

function ChamadaAjax(codigos, url, dropdownlist, selecionarautomatico, valorautomatico, campos) {
    $.ajax({
        url: url,
        type: "POST",
        data: {
            param1: codigos
        },
        success: function (data) {
            if (data.retorno1 != null) {
                $.each(data.retorno1, function (index, itemData) {
                    dropdownlist.append($('<option/>', {
                        value: itemData.Value,
                        text: itemData.Text
                    }));
                });
            }

            if (valorautomatico != 0)
            {
                var desmarcar = ItemSelecionadoDropDownList(selecionarautomatico);
                $("#" + selecionarautomatico).find("" + "option:eq(" + desmarcar + ")" + "").attr("selected", false);
                $("#" + selecionarautomatico).find("" + "option:eq(" + valorautomatico + ")" + "").attr("selected", true);
            }

            if (data.retorno2 != null) {
                var x = campos.length;
                var table = '<table class="table">';

                for (i = 0; i < campos.length; i++) {
                    if (i == 0)
                        table = table + '<tr><th>' + campos[i] + '</th>';
                    else
                        table = table + '<th>' + campos[i] + '</th>';
                }
                table = table + '</tr>';

                $.each(data.retorno2, function (index, itemData) {
                    for (i = 0; i < campos.length; i++) {
                        if (i == 0)
                            table = table + '<tr><td>' + itemData['Campo'+(i+1)] + '</td>';
                        else
                            table = table + '<td>' + itemData['Campo'+(i+1)] + '</td>';
                    }
                    table = table + '</tr>';
                });
                table = table + '</table>';
                document.getElementById("listaCampos").innerHTML = table;
                return true;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(document).ajaxStop(function () {
                Mensagem.Alert({
                    titulo: "Erro",
                    mensagem: "Ocorreu um erro durante o processo: " + errorThrown,
                    tipo: "Erro",
                    botao: "Fechar"
                });
            });
            return false;
        }
    });
}

function ChamadaAjaxRetornoString(codigos, url, campo) {
    $.ajax({
        url: url,
        type: "POST",
        data: {
            param1: codigos
        },
        success: function (data) {

            if (data.retorno != null)
            {
                $("#" + campo).val(data.retorno);
                return true;
            }
            else
                return false;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(document).ajaxStop(function () {
                Mensagem.Alert({
                    titulo: "Erro",
                    mensagem: "Ocorreu um erro durante o processo: " + errorThrown,
                    tipo: "Erro",
                    botao: "Fechar"
                });
            });
            return "";
        }
    });
}

