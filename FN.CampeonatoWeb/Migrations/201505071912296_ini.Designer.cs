// <auto-generated />
namespace FN.CampeonatoWeb.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ini : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ini));
        
        string IMigrationMetadata.Id
        {
            get { return "201505071912296_ini"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
