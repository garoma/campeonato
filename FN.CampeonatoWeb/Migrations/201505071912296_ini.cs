namespace FN.CampeonatoWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ini : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Arbitro",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(maxLength: 150),
                        Nascimento = c.DateTime(nullable: false),
                        Tipo = c.Int(nullable: false),
                        Foto = c.Binary(),
                        ArbitroCampeonato_Id = c.Int(),
                        ArbitroTemporada_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ArbitroCampeonato", t => t.ArbitroCampeonato_Id)
                .ForeignKey("dbo.ArbitroTemporada", t => t.ArbitroTemporada_Id)
                .Index(t => t.ArbitroCampeonato_Id)
                .Index(t => t.ArbitroTemporada_Id);
            
            CreateTable(
                "dbo.ArbitroCampeonato",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CampeonatoId = c.Int(nullable: false),
                        ArbitroId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Arbitro", t => t.ArbitroId, cascadeDelete: true)
                .ForeignKey("dbo.Campeonato", t => t.CampeonatoId, cascadeDelete: true)
                .Index(t => t.CampeonatoId)
                .Index(t => t.ArbitroId);
            
            CreateTable(
                "dbo.Campeonato",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 150),
                        TipoCampeonatoId = c.Int(nullable: false),
                        Pais = c.String(maxLength: 30),
                        ArbitroCampeonato_Id = c.Int(),
                        ArbitroTemporada_Id = c.Int(),
                        CampeonatoTemporada_Id = c.Int(),
                        EquipeCampeonato_Id = c.Int(),
                        EquipeTemporada_Id = c.Int(),
                        EstruturaTemporada_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TipoCampeonato", t => t.TipoCampeonatoId, cascadeDelete: true)
                .ForeignKey("dbo.ArbitroCampeonato", t => t.ArbitroCampeonato_Id)
                .ForeignKey("dbo.ArbitroTemporada", t => t.ArbitroTemporada_Id)
                .ForeignKey("dbo.CampeonatoTemporada", t => t.CampeonatoTemporada_Id)
                .ForeignKey("dbo.EquipeCampeonato", t => t.EquipeCampeonato_Id)
                .ForeignKey("dbo.EquipeTemporada", t => t.EquipeTemporada_Id)
                .ForeignKey("dbo.EstruturaTemporada", t => t.EstruturaTemporada_Id)
                .Index(t => t.TipoCampeonatoId)
                .Index(t => t.ArbitroCampeonato_Id)
                .Index(t => t.ArbitroTemporada_Id)
                .Index(t => t.CampeonatoTemporada_Id)
                .Index(t => t.EquipeCampeonato_Id)
                .Index(t => t.EquipeTemporada_Id)
                .Index(t => t.EstruturaTemporada_Id);
            
            CreateTable(
                "dbo.TipoCampeonato",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 100),
                        Campeonato_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campeonato", t => t.Campeonato_Id)
                .Index(t => t.Campeonato_Id);
            
            CreateTable(
                "dbo.ArbitroTemporada",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CampeonatoId = c.Int(nullable: false),
                        TemporadaId = c.Int(nullable: false),
                        ArbitroId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Arbitro", t => t.ArbitroId, cascadeDelete: true)
                .ForeignKey("dbo.Campeonato", t => t.CampeonatoId, cascadeDelete: true)
                .ForeignKey("dbo.Temporada", t => t.TemporadaId, cascadeDelete: true)
                .Index(t => t.CampeonatoId)
                .Index(t => t.TemporadaId)
                .Index(t => t.ArbitroId);
            
            CreateTable(
                "dbo.Temporada",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 150),
                        FormulaDisputa = c.String(),
                        PontosVitoria = c.Int(nullable: false),
                        PontosEmpate = c.Int(nullable: false),
                        PontosVitoriaProrrogacao = c.Int(nullable: false),
                        PontosEmpateProgrrogacao = c.Int(nullable: false),
                        PontosDerrotaProrrogacao = c.Int(nullable: false),
                        PontosVitoriaPenaltis = c.Int(nullable: false),
                        PontosDerrotaPenaltis = c.Int(nullable: false),
                        CriterioDesempate = c.String(maxLength: 100),
                        NumTimePromovido = c.Int(nullable: false),
                        NumTimeCompete1 = c.Int(nullable: false),
                        NumTimeCompete2 = c.Int(nullable: false),
                        NumTimeRebaixamento = c.Int(nullable: false),
                        NumTimeRebaixado = c.Int(nullable: false),
                        ArbitroTemporada_Id = c.Int(),
                        CampeonatoTemporada_Id = c.Int(),
                        EquipeTemporada_Id = c.Int(),
                        EstruturaTemporada_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ArbitroTemporada", t => t.ArbitroTemporada_Id)
                .ForeignKey("dbo.CampeonatoTemporada", t => t.CampeonatoTemporada_Id)
                .ForeignKey("dbo.EquipeTemporada", t => t.EquipeTemporada_Id)
                .ForeignKey("dbo.EstruturaTemporada", t => t.EstruturaTemporada_Id)
                .Index(t => t.ArbitroTemporada_Id)
                .Index(t => t.CampeonatoTemporada_Id)
                .Index(t => t.EquipeTemporada_Id)
                .Index(t => t.EstruturaTemporada_Id);
            
            CreateTable(
                "dbo.CampeonatoTemporada",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TemporadaId = c.Int(nullable: false),
                        CampeonatoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campeonato", t => t.CampeonatoId, cascadeDelete: true)
                .ForeignKey("dbo.Temporada", t => t.TemporadaId, cascadeDelete: true)
                .Index(t => t.TemporadaId)
                .Index(t => t.CampeonatoId);
            
            CreateTable(
                "dbo.Classificacao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TemporadaId = c.Int(nullable: false),
                        DataJogo = c.DateTime(nullable: false),
                        EquipeId = c.Int(nullable: false),
                        Pontos = c.Int(nullable: false),
                        Vitorias = c.Int(nullable: false),
                        Empates = c.Int(nullable: false),
                        Derrotas = c.Int(nullable: false),
                        VitoriasProrrogacao = c.Int(nullable: false),
                        VitoriasPenaltis = c.Int(nullable: false),
                        DerrotaProrrogacao = c.Int(nullable: false),
                        DerrotaPenaltis = c.Int(nullable: false),
                        GolsPro = c.Int(nullable: false),
                        GolsContra = c.Int(nullable: false),
                        Publico = c.Int(nullable: false),
                        Local = c.String(maxLength: 1),
                        Jogos = c.Int(nullable: false),
                        Realizado = c.Int(nullable: false),
                        EmpateProrrogacao = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Equipe", t => t.EquipeId, cascadeDelete: true)
                .ForeignKey("dbo.Temporada", t => t.TemporadaId, cascadeDelete: true)
                .Index(t => t.TemporadaId)
                .Index(t => t.EquipeId);
            
            CreateTable(
                "dbo.Equipe",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 150),
                        ClubeId = c.Int(nullable: false),
                        TipoEsporteId = c.Int(nullable: false),
                        TipoEquipeId = c.Int(nullable: false),
                        EquipeCampeonato_Id = c.Int(),
                        EquipeJogador_Id = c.Int(),
                        EquipeTemporada_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clube", t => t.ClubeId, cascadeDelete: true)
                .ForeignKey("dbo.TipoEquipe", t => t.TipoEquipeId, cascadeDelete: true)
                .ForeignKey("dbo.TipoEsporte", t => t.TipoEsporteId, cascadeDelete: true)
                .ForeignKey("dbo.EquipeCampeonato", t => t.EquipeCampeonato_Id)
                .ForeignKey("dbo.EquipeJogador", t => t.EquipeJogador_Id)
                .ForeignKey("dbo.EquipeTemporada", t => t.EquipeTemporada_Id)
                .Index(t => t.ClubeId)
                .Index(t => t.TipoEsporteId)
                .Index(t => t.TipoEquipeId)
                .Index(t => t.EquipeCampeonato_Id)
                .Index(t => t.EquipeJogador_Id)
                .Index(t => t.EquipeTemporada_Id);
            
            CreateTable(
                "dbo.Clube",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(maxLength: 30),
                        Descricao = c.String(maxLength: 150),
                        DataFundacao = c.DateTime(nullable: false),
                        Cidade = c.String(maxLength: 50),
                        Pais = c.String(maxLength: 50),
                        Foto = c.Binary(),
                        EstadioId = c.Int(nullable: false),
                        Equipe_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Estadio", t => t.EstadioId, cascadeDelete: true)
                .ForeignKey("dbo.Equipe", t => t.Equipe_Id)
                .Index(t => t.EstadioId)
                .Index(t => t.Equipe_Id);
            
            CreateTable(
                "dbo.Estadio",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(maxLength: 50),
                        Descricao = c.String(maxLength: 200),
                        Capacidade = c.Int(nullable: false),
                        Foto = c.Binary(),
                        Pais = c.String(maxLength: 50),
                        Clube_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clube", t => t.Clube_Id)
                .Index(t => t.Clube_Id);
            
            CreateTable(
                "dbo.TipoEquipe",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 100),
                        Equipe_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Equipe", t => t.Equipe_Id)
                .Index(t => t.Equipe_Id);
            
            CreateTable(
                "dbo.TipoEsporte",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 100),
                        Equipe_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Equipe", t => t.Equipe_Id)
                .Index(t => t.Equipe_Id);
            
            CreateTable(
                "dbo.Content",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        Contents = c.String(),
                        Image = c.Binary(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EquipeCampeonato",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CampeonatoId = c.Int(nullable: false),
                        EquipeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campeonato", t => t.CampeonatoId, cascadeDelete: true)
                .ForeignKey("dbo.Equipe", t => t.EquipeId, cascadeDelete: true)
                .Index(t => t.CampeonatoId)
                .Index(t => t.EquipeId);
            
            CreateTable(
                "dbo.EquipeJogador",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EquipeId = c.Int(nullable: false),
                        JogadorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Equipe", t => t.EquipeId, cascadeDelete: true)
                .ForeignKey("dbo.Jogador", t => t.JogadorId, cascadeDelete: true)
                .Index(t => t.EquipeId)
                .Index(t => t.JogadorId);
            
            CreateTable(
                "dbo.Jogador",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Apelido = c.String(maxLength: 30),
                        Nome = c.String(maxLength: 150),
                        NumeroCamisa = c.Int(nullable: false),
                        PosicaoJogadorId = c.Int(nullable: false),
                        Nascimento = c.DateTime(nullable: false),
                        EMail = c.String(maxLength: 150),
                        Nacionalidade = c.String(maxLength: 50),
                        Foto = c.Binary(),
                        EquipeJogador_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PosicaoJogador", t => t.PosicaoJogadorId, cascadeDelete: true)
                .ForeignKey("dbo.EquipeJogador", t => t.EquipeJogador_Id)
                .Index(t => t.PosicaoJogadorId)
                .Index(t => t.EquipeJogador_Id);
            
            CreateTable(
                "dbo.PosicaoJogador",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 100),
                        Jogador_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Jogador", t => t.Jogador_Id)
                .Index(t => t.Jogador_Id);
            
            CreateTable(
                "dbo.EquipeTemporada",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CampeonatoId = c.Int(nullable: false),
                        TemporadaId = c.Int(nullable: false),
                        EquipeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campeonato", t => t.CampeonatoId, cascadeDelete: true)
                .ForeignKey("dbo.Equipe", t => t.EquipeId, cascadeDelete: true)
                .ForeignKey("dbo.Temporada", t => t.TemporadaId, cascadeDelete: true)
                .Index(t => t.CampeonatoId)
                .Index(t => t.TemporadaId)
                .Index(t => t.EquipeId);
            
            CreateTable(
                "dbo.EstruturaTemporada",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CampeonatoId = c.Int(nullable: false),
                        TemporadaId = c.Int(nullable: false),
                        FaseId = c.Int(nullable: false),
                        GrupoId = c.Int(nullable: false),
                        RodadaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campeonato", t => t.CampeonatoId, cascadeDelete: true)
                .ForeignKey("dbo.Fase", t => t.FaseId, cascadeDelete: true)
                .ForeignKey("dbo.Grupo", t => t.GrupoId, cascadeDelete: true)
                .ForeignKey("dbo.Rodada", t => t.RodadaId, cascadeDelete: true)
                .ForeignKey("dbo.Temporada", t => t.TemporadaId, cascadeDelete: true)
                .Index(t => t.CampeonatoId)
                .Index(t => t.TemporadaId)
                .Index(t => t.FaseId)
                .Index(t => t.GrupoId)
                .Index(t => t.RodadaId);
            
            CreateTable(
                "dbo.Fase",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 100),
                        EstruturaTemporada_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EstruturaTemporada", t => t.EstruturaTemporada_Id)
                .Index(t => t.EstruturaTemporada_Id);
            
            CreateTable(
                "dbo.Grupo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 100),
                        EstruturaTemporada_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EstruturaTemporada", t => t.EstruturaTemporada_Id)
                .Index(t => t.EstruturaTemporada_Id);
            
            CreateTable(
                "dbo.Rodada",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 100),
                        EstruturaTemporada_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EstruturaTemporada", t => t.EstruturaTemporada_Id)
                .Index(t => t.EstruturaTemporada_Id);
            
            CreateTable(
                "dbo.Evento",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.JogoArbitro",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        JogoId = c.Int(nullable: false),
                        ArbitroId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Arbitro", t => t.ArbitroId, cascadeDelete: true)
                .ForeignKey("dbo.JogoGeral", t => t.JogoId, cascadeDelete: true)
                .Index(t => t.JogoId)
                .Index(t => t.ArbitroId);
            
            CreateTable(
                "dbo.JogoGeral",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DataJogo = c.DateTime(nullable: false),
                        HoraJogo = c.String(maxLength: 5),
                        EstadioId = c.Int(nullable: false),
                        Comentarios = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Estadio", t => t.EstadioId, cascadeDelete: true)
                .Index(t => t.EstadioId);
            
            CreateTable(
                "dbo.JogoEquipe",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        JogoId = c.Int(nullable: false),
                        Tipo = c.Int(nullable: false),
                        EquipeId = c.Int(nullable: false),
                        Gols = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Equipe", t => t.EquipeId, cascadeDelete: false)
                .ForeignKey("dbo.JogoGeral", t => t.JogoId, cascadeDelete: true)
                .Index(t => t.JogoId)
                .Index(t => t.EquipeId);
            
            CreateTable(
                "dbo.JogoEvento",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        JogoId = c.Int(nullable: false),
                        JogadorId = c.Int(nullable: false),
                        EventoId = c.Int(nullable: false),
                        TempoId = c.Int(nullable: false),
                        Minuto = c.Int(nullable: false),
                        Quantidade = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Evento", t => t.EventoId, cascadeDelete: true)
                .ForeignKey("dbo.Jogador", t => t.JogadorId, cascadeDelete: true)
                .ForeignKey("dbo.JogoGeral", t => t.JogoId, cascadeDelete: true)
                .ForeignKey("dbo.Tempo", t => t.TempoId, cascadeDelete: true)
                .Index(t => t.JogoId)
                .Index(t => t.JogadorId)
                .Index(t => t.EventoId)
                .Index(t => t.TempoId);
            
            CreateTable(
                "dbo.Tempo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Lance",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 150),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Punicao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 150),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.JogoEvento", "TempoId", "dbo.Tempo");
            DropForeignKey("dbo.JogoEvento", "JogoId", "dbo.JogoGeral");
            DropForeignKey("dbo.JogoEvento", "JogadorId", "dbo.Jogador");
            DropForeignKey("dbo.JogoEvento", "EventoId", "dbo.Evento");
            DropForeignKey("dbo.JogoEquipe", "JogoId", "dbo.JogoGeral");
            DropForeignKey("dbo.JogoEquipe", "EquipeId", "dbo.Equipe");
            DropForeignKey("dbo.JogoArbitro", "JogoId", "dbo.JogoGeral");
            DropForeignKey("dbo.JogoGeral", "EstadioId", "dbo.Estadio");
            DropForeignKey("dbo.JogoArbitro", "ArbitroId", "dbo.Arbitro");
            DropForeignKey("dbo.Temporada", "EstruturaTemporada_Id", "dbo.EstruturaTemporada");
            DropForeignKey("dbo.EstruturaTemporada", "TemporadaId", "dbo.Temporada");
            DropForeignKey("dbo.Rodada", "EstruturaTemporada_Id", "dbo.EstruturaTemporada");
            DropForeignKey("dbo.EstruturaTemporada", "RodadaId", "dbo.Rodada");
            DropForeignKey("dbo.Grupo", "EstruturaTemporada_Id", "dbo.EstruturaTemporada");
            DropForeignKey("dbo.EstruturaTemporada", "GrupoId", "dbo.Grupo");
            DropForeignKey("dbo.Fase", "EstruturaTemporada_Id", "dbo.EstruturaTemporada");
            DropForeignKey("dbo.EstruturaTemporada", "FaseId", "dbo.Fase");
            DropForeignKey("dbo.Campeonato", "EstruturaTemporada_Id", "dbo.EstruturaTemporada");
            DropForeignKey("dbo.EstruturaTemporada", "CampeonatoId", "dbo.Campeonato");
            DropForeignKey("dbo.Temporada", "EquipeTemporada_Id", "dbo.EquipeTemporada");
            DropForeignKey("dbo.EquipeTemporada", "TemporadaId", "dbo.Temporada");
            DropForeignKey("dbo.Equipe", "EquipeTemporada_Id", "dbo.EquipeTemporada");
            DropForeignKey("dbo.EquipeTemporada", "EquipeId", "dbo.Equipe");
            DropForeignKey("dbo.Campeonato", "EquipeTemporada_Id", "dbo.EquipeTemporada");
            DropForeignKey("dbo.EquipeTemporada", "CampeonatoId", "dbo.Campeonato");
            DropForeignKey("dbo.Jogador", "EquipeJogador_Id", "dbo.EquipeJogador");
            DropForeignKey("dbo.EquipeJogador", "JogadorId", "dbo.Jogador");
            DropForeignKey("dbo.PosicaoJogador", "Jogador_Id", "dbo.Jogador");
            DropForeignKey("dbo.Jogador", "PosicaoJogadorId", "dbo.PosicaoJogador");
            DropForeignKey("dbo.Equipe", "EquipeJogador_Id", "dbo.EquipeJogador");
            DropForeignKey("dbo.EquipeJogador", "EquipeId", "dbo.Equipe");
            DropForeignKey("dbo.Equipe", "EquipeCampeonato_Id", "dbo.EquipeCampeonato");
            DropForeignKey("dbo.EquipeCampeonato", "EquipeId", "dbo.Equipe");
            DropForeignKey("dbo.Campeonato", "EquipeCampeonato_Id", "dbo.EquipeCampeonato");
            DropForeignKey("dbo.EquipeCampeonato", "CampeonatoId", "dbo.Campeonato");
            DropForeignKey("dbo.Classificacao", "TemporadaId", "dbo.Temporada");
            DropForeignKey("dbo.Classificacao", "EquipeId", "dbo.Equipe");
            DropForeignKey("dbo.TipoEsporte", "Equipe_Id", "dbo.Equipe");
            DropForeignKey("dbo.TipoEquipe", "Equipe_Id", "dbo.Equipe");
            DropForeignKey("dbo.Equipe", "TipoEsporteId", "dbo.TipoEsporte");
            DropForeignKey("dbo.Equipe", "TipoEquipeId", "dbo.TipoEquipe");
            DropForeignKey("dbo.Clube", "Equipe_Id", "dbo.Equipe");
            DropForeignKey("dbo.Equipe", "ClubeId", "dbo.Clube");
            DropForeignKey("dbo.Estadio", "Clube_Id", "dbo.Clube");
            DropForeignKey("dbo.Clube", "EstadioId", "dbo.Estadio");
            DropForeignKey("dbo.Temporada", "CampeonatoTemporada_Id", "dbo.CampeonatoTemporada");
            DropForeignKey("dbo.CampeonatoTemporada", "TemporadaId", "dbo.Temporada");
            DropForeignKey("dbo.Campeonato", "CampeonatoTemporada_Id", "dbo.CampeonatoTemporada");
            DropForeignKey("dbo.CampeonatoTemporada", "CampeonatoId", "dbo.Campeonato");
            DropForeignKey("dbo.Temporada", "ArbitroTemporada_Id", "dbo.ArbitroTemporada");
            DropForeignKey("dbo.ArbitroTemporada", "TemporadaId", "dbo.Temporada");
            DropForeignKey("dbo.Campeonato", "ArbitroTemporada_Id", "dbo.ArbitroTemporada");
            DropForeignKey("dbo.ArbitroTemporada", "CampeonatoId", "dbo.Campeonato");
            DropForeignKey("dbo.Arbitro", "ArbitroTemporada_Id", "dbo.ArbitroTemporada");
            DropForeignKey("dbo.ArbitroTemporada", "ArbitroId", "dbo.Arbitro");
            DropForeignKey("dbo.Campeonato", "ArbitroCampeonato_Id", "dbo.ArbitroCampeonato");
            DropForeignKey("dbo.ArbitroCampeonato", "CampeonatoId", "dbo.Campeonato");
            DropForeignKey("dbo.TipoCampeonato", "Campeonato_Id", "dbo.Campeonato");
            DropForeignKey("dbo.Campeonato", "TipoCampeonatoId", "dbo.TipoCampeonato");
            DropForeignKey("dbo.Arbitro", "ArbitroCampeonato_Id", "dbo.ArbitroCampeonato");
            DropForeignKey("dbo.ArbitroCampeonato", "ArbitroId", "dbo.Arbitro");
            DropIndex("dbo.JogoEvento", new[] { "TempoId" });
            DropIndex("dbo.JogoEvento", new[] { "EventoId" });
            DropIndex("dbo.JogoEvento", new[] { "JogadorId" });
            DropIndex("dbo.JogoEvento", new[] { "JogoId" });
            DropIndex("dbo.JogoEquipe", new[] { "EquipeId" });
            DropIndex("dbo.JogoEquipe", new[] { "JogoId" });
            DropIndex("dbo.JogoGeral", new[] { "EstadioId" });
            DropIndex("dbo.JogoArbitro", new[] { "ArbitroId" });
            DropIndex("dbo.JogoArbitro", new[] { "JogoId" });
            DropIndex("dbo.Rodada", new[] { "EstruturaTemporada_Id" });
            DropIndex("dbo.Grupo", new[] { "EstruturaTemporada_Id" });
            DropIndex("dbo.Fase", new[] { "EstruturaTemporada_Id" });
            DropIndex("dbo.EstruturaTemporada", new[] { "RodadaId" });
            DropIndex("dbo.EstruturaTemporada", new[] { "GrupoId" });
            DropIndex("dbo.EstruturaTemporada", new[] { "FaseId" });
            DropIndex("dbo.EstruturaTemporada", new[] { "TemporadaId" });
            DropIndex("dbo.EstruturaTemporada", new[] { "CampeonatoId" });
            DropIndex("dbo.EquipeTemporada", new[] { "EquipeId" });
            DropIndex("dbo.EquipeTemporada", new[] { "TemporadaId" });
            DropIndex("dbo.EquipeTemporada", new[] { "CampeonatoId" });
            DropIndex("dbo.PosicaoJogador", new[] { "Jogador_Id" });
            DropIndex("dbo.Jogador", new[] { "EquipeJogador_Id" });
            DropIndex("dbo.Jogador", new[] { "PosicaoJogadorId" });
            DropIndex("dbo.EquipeJogador", new[] { "JogadorId" });
            DropIndex("dbo.EquipeJogador", new[] { "EquipeId" });
            DropIndex("dbo.EquipeCampeonato", new[] { "EquipeId" });
            DropIndex("dbo.EquipeCampeonato", new[] { "CampeonatoId" });
            DropIndex("dbo.TipoEsporte", new[] { "Equipe_Id" });
            DropIndex("dbo.TipoEquipe", new[] { "Equipe_Id" });
            DropIndex("dbo.Estadio", new[] { "Clube_Id" });
            DropIndex("dbo.Clube", new[] { "Equipe_Id" });
            DropIndex("dbo.Clube", new[] { "EstadioId" });
            DropIndex("dbo.Equipe", new[] { "EquipeTemporada_Id" });
            DropIndex("dbo.Equipe", new[] { "EquipeJogador_Id" });
            DropIndex("dbo.Equipe", new[] { "EquipeCampeonato_Id" });
            DropIndex("dbo.Equipe", new[] { "TipoEquipeId" });
            DropIndex("dbo.Equipe", new[] { "TipoEsporteId" });
            DropIndex("dbo.Equipe", new[] { "ClubeId" });
            DropIndex("dbo.Classificacao", new[] { "EquipeId" });
            DropIndex("dbo.Classificacao", new[] { "TemporadaId" });
            DropIndex("dbo.CampeonatoTemporada", new[] { "CampeonatoId" });
            DropIndex("dbo.CampeonatoTemporada", new[] { "TemporadaId" });
            DropIndex("dbo.Temporada", new[] { "EstruturaTemporada_Id" });
            DropIndex("dbo.Temporada", new[] { "EquipeTemporada_Id" });
            DropIndex("dbo.Temporada", new[] { "CampeonatoTemporada_Id" });
            DropIndex("dbo.Temporada", new[] { "ArbitroTemporada_Id" });
            DropIndex("dbo.ArbitroTemporada", new[] { "ArbitroId" });
            DropIndex("dbo.ArbitroTemporada", new[] { "TemporadaId" });
            DropIndex("dbo.ArbitroTemporada", new[] { "CampeonatoId" });
            DropIndex("dbo.TipoCampeonato", new[] { "Campeonato_Id" });
            DropIndex("dbo.Campeonato", new[] { "EstruturaTemporada_Id" });
            DropIndex("dbo.Campeonato", new[] { "EquipeTemporada_Id" });
            DropIndex("dbo.Campeonato", new[] { "EquipeCampeonato_Id" });
            DropIndex("dbo.Campeonato", new[] { "CampeonatoTemporada_Id" });
            DropIndex("dbo.Campeonato", new[] { "ArbitroTemporada_Id" });
            DropIndex("dbo.Campeonato", new[] { "ArbitroCampeonato_Id" });
            DropIndex("dbo.Campeonato", new[] { "TipoCampeonatoId" });
            DropIndex("dbo.ArbitroCampeonato", new[] { "ArbitroId" });
            DropIndex("dbo.ArbitroCampeonato", new[] { "CampeonatoId" });
            DropIndex("dbo.Arbitro", new[] { "ArbitroTemporada_Id" });
            DropIndex("dbo.Arbitro", new[] { "ArbitroCampeonato_Id" });
            DropTable("dbo.Punicao");
            DropTable("dbo.Lance");
            DropTable("dbo.Tempo");
            DropTable("dbo.JogoEvento");
            DropTable("dbo.JogoEquipe");
            DropTable("dbo.JogoGeral");
            DropTable("dbo.JogoArbitro");
            DropTable("dbo.Evento");
            DropTable("dbo.Rodada");
            DropTable("dbo.Grupo");
            DropTable("dbo.Fase");
            DropTable("dbo.EstruturaTemporada");
            DropTable("dbo.EquipeTemporada");
            DropTable("dbo.PosicaoJogador");
            DropTable("dbo.Jogador");
            DropTable("dbo.EquipeJogador");
            DropTable("dbo.EquipeCampeonato");
            DropTable("dbo.Content");
            DropTable("dbo.TipoEsporte");
            DropTable("dbo.TipoEquipe");
            DropTable("dbo.Estadio");
            DropTable("dbo.Clube");
            DropTable("dbo.Equipe");
            DropTable("dbo.Classificacao");
            DropTable("dbo.CampeonatoTemporada");
            DropTable("dbo.Temporada");
            DropTable("dbo.ArbitroTemporada");
            DropTable("dbo.TipoCampeonato");
            DropTable("dbo.Campeonato");
            DropTable("dbo.ArbitroCampeonato");
            DropTable("dbo.Arbitro");
        }
    }
}
