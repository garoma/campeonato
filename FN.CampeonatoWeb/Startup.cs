﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FN.CampeonatoWeb.Startup))]
namespace FN.CampeonatoWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
