﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FN.CampeonatoWeb.Helper
{
	public class Enum
	{
	}

    public enum TipoArbitro
    {
        Primeiro = 1,
        Segundo = 2,
        Terceiro = 3,
        Quarto = 4,
        Auxiliar = 5,
        Bandeirinha = 6
    }

    public enum EstadioJogo
    {
        Casa = 1,
        Fora = 2
    }

}